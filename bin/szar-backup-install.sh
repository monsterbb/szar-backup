#!/bin/bash

set -e

. $(dirname $0)/szar-backup-install.inc

apt-get update
apt-get install --no-install-recommends -y $DEB_PACKAGES
cpanm $CPANM_PACKAGES
apt-get -y remove cpanminus make
apt-get -y autoremove
rm -rf /var/lib/apt/lists/*
