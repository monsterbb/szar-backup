package Archive::SzarStream;
require 5.005_03;

use warnings;
use strict;
use Compress::Raw::Bzip2 ;
use Compress::Raw::Zlib ;

use IO::Select;
use Data::Dumper;
use Fcntl ':mode';
use File::Path qw(make_path);

use constant COMPRESS_NONE  => "SZAR";
use constant COMPRESS_GZIP  => "GZ";
use constant COMPRESS_BZIP2  => "BZ2";
use constant DEVNULL  => '/dev/null';

use constant READ_BUFFER => 32768;
use constant SZARVERSION => "00";
use constant SZARBEGIN => "SZAR";
use constant SZAREND  => "SZAR--";
use constant HEAD  => 6;

use base 'Exporter';
our @EXPORT                 = qw[ COMPRESS_GZIP COMPRESS_BZIP2 ];		 



sub _defaults {
 my %obj = (
      "debug"=>0,
      "overwrite"=>0,
      "timeout"=>0,
      "dir"=>"",
      "do_chmod_files"=>1,
      "do_chmod_dirs"=>0,
      "do_chown"=>0,	 
      "do_utime"=>1,	 
      "compression"=> COMPRESS_NONE,
      "compression_level"=>9, # for gzip only
      "read_bytes"=>0,
      "sent_bytes"=>0,
 );
 return \%obj;
}
sub setcompressionbyfilename {
  my ($class, $filename, $hash) = @_;
  %$hash = () if((!$hash)||(ref $hash ne "HASH"));
  if(!defined($hash->{'compression'})) {
     if($filename =~ /\.bz2$/i) {
       $hash->{'compression'} = COMPRESS_BZIP2;
     } 
     elsif($filename =~ /\.(gz|z)$/i) {
       $hash->{'compression'} = COMPRESS_GZIP;
     } 

  }
}
sub write_file {
    my $class = shift;
    my $filename = shift;
    my $hash  = shift;
    
    open(my $fh, ">$filename") or die("Cant open $filename: $!");
    binmode $fh;
    $hash->{'timeout'} = 0; # it wouldnt work for files on windows anyway
    $class->setcompressionbyfilename($filename, $hash);
    return $class->write_handle($fh, $hash);	
}
sub _common {
  my $obj= shift;
  my $fh = shift;
    if($obj->{'dir'}) {
      $obj->{'dir'} =~ s/\/{2,}/\//g;
      $obj->{'dir'} =~ s/\/+$/\//g;
      $obj->{'dir'} .= "/";
    }
    $obj->{'fh'} = $fh;
    if($obj->{'timeout'}) {
      $obj->{'select'} = IO::Select->new($obj->{'fh'});
    }  
}
sub write_handle {
    my $class = shift;
    my $fh = shift;
    my $hash  = shift;
    my $obj = _defaults();
    for my $k (keys %$hash) {
       die("Unknown option: $k") if(!defined($obj->{$k}));
       $obj->{$k} = $hash->{$k};
    }
    bless $obj, $class;
    $obj->_common($fh);
    $obj->_get_write_handle();
    
    return $obj;
}
sub read_file {
    my $class = shift;
    my $filename = shift;
    my $hash  = shift;
    
    open(my $fh, "<$filename") or die("Cant open $filename: $!");
    binmode $fh;
    $hash->{'timeout'} = 0; # it wouldnt work for files on windows anyway
    $class->setcompressionbyfilename($filename, $hash);
    return $class->read_handle($fh, $hash);	
}
sub read_handle {
    my $class = shift;
    my $fh = shift;
    my $hash  = shift;
    my $obj = _defaults();
    for my $k (keys %$hash) {
       die("Unknown option: $k") if(!defined($obj->{$k}));
       $obj->{$k} = $hash->{$k};
    }
    bless $obj, $class;
    $obj->_common($fh);
    $obj->_get_read_handle();
    $obj->{'lbuf'} = "";

    return $obj;
}
sub genof {
  my $class = shift;
  my $e = shift;
  my $saveto = shift;
  return $saveto if($saveto);
  return "$class->{'dir'}/$e->{'filename'}" if($class->{'dir'});
  return $e->{'filename'}
}
sub extract {
  my $class = shift;
  my $saveto = shift;
  
  die "No last entry... did you use read_entry?" if(!$class->{'lastentry'});
  my $e = $class->{'lastentry'}; # shortcut
  undef $class->{'lastentry'};
  
  my $of = $class->genof($e, $saveto);
  my $sikeres = 1;

  if(-l $of) {
    $sikeres = 0;
    warn "Destination file $of already exists, and it is a link. Its nasty, skipping.";
    $of = DEVNULL;
  }
  elsif(($e->{'t'} !~ /^(D|R)$/)&&(-e $of)) {
    if($class->{'overwrite'}) {
      unlink($of) or warn "unlink of $of failed: $!";
    }
    else{
      $sikeres = 0;	
      $of = DEVNULL;  
    }
  }

  if($of ne DEVNULL) {
    if($of =~ /(.+)\/.+/) {
       eval { make_path($1);}; warn $@ if($@);
    }
  }

  if($e->{'t'} eq "F") {
    # plain file.
    my $s;
    my $o;
    if($of ne DEVNULL) {
      open($o, ">$of");
      if(!$o) {
        warn "Cant open $of for writing: $!";
	$sikeres = 0;
      }
    }

    binmode $o if($o);
    my $hatravan = $e->{'filesize'};
    while($hatravan) {
      my $actread = ($hatravan > READ_BUFFER ? READ_BUFFER : $hatravan);
      my $abuf;
      my $ar = $class->_myread(\$abuf, $actread);
      $hatravan -= $ar;
      
      if($o) {
        my $suc = print $o $abuf;
	if(!$suc) {
	  warn "Couldnt write chunk while extracting to $of: $!";
	  close $o;
	  undef $o;
          undef($of);
	  $sikeres = 0; 
	}
      }
    }
    close($o) if($o);	
  }
  elsif($e->{'t'} eq "D") {
    if(($of ne DEVNULL)&&(!-d $of)) {
       eval { make_path($of) or $sikeres = 0; }; if($@) { warn $@; $sikeres = 0; }
    }
  }
  elsif($e->{'t'} eq "L") {
    # mindenkepp kiolvassuk a tartalmat
    
    my $pointsto;
    my $n = $class->_myread(\$pointsto, $e->{'filesize'});
    die "Couldnt read symlink data" if($n != $e->{'filesize'});
    if($of ne DEVNULL) {
       eval { symlink($pointsto, $of); }; if($@) { warn $@ ; $sikeres = 0; }
    }
  }
  elsif($e->{'t'} eq "R") {
    # this is a repair entry
    if($of ne DEVNULL) {
       eval { truncate($of, $e->{'filesize'}); }; if($@) { warn $@; $sikeres = 0; } 
    }
  }
  elsif($e->{'t'} eq "O") {
     eval {
          die "Mknod is not supported on Windows" if($^O eq "MSWin32");
          require Unix::Mknod;

          Unix::Mknod::mknod($of, $e->{'mode'}, Unix::Mknod::makedev(Unix::Mknod::major($e->{'rdev'}),Unix::Mknod::minor($e->{'rdev'})));
     };
     if($@) { warn $@; $sikeres = 0; } 
  }
  elsif($e->{'t'} eq "P") {
    #phantom entry, used only for file modes, permissions, times
  }
  else { 
    die "Unsupported file type (this case cant happen)";
  }

  if($of ne DEVNULL) {  
      # restoring permissions and shit
      if((($class->{'do_chmod_files'})&&($e->{'t'} ne "D"))||(($class->{'do_chmod_dirs'})&&($e->{'t'} eq "D")))  {
	chmod $e->{'mode'}, $of or warn "chmod of $of to $e->{'mode'} failed";
      }
      if($class->{'do_chown'}) {
	chown $e->{'uid'}, $e->{'gid'}, $of or warn "chown of $of to $e->{'uid'} $e->{'gid'} failed";
      }
      if($class->{'do_utime'}) {
	utime $e->{'atime'}, $e->{'mtime'}, $of or warn "utime of $of failed";
      }
  }

  return $sikeres;
}
sub skip {
  my $class = shift;
  $class->extract(DEVNULL);
}

sub write_entry {
  my $class = shift;
  my $ofilename = shift;
  die "You didnt open szar for writing" if(!$class->{'wfh'});

  my $filename = $ofilename;
  $filename =~ s/\/{2,}/\//g;
  $filename =~ s/\/+$//g;
  $filename = $class->{'dir'}.$filename;
  
  my @stat = @_;
  @stat = lstat($filename) if(!scalar @stat);
  die("Cant stat file") if(!scalar @stat) ;
  
  my ($dev,$ino,$mode,$nlink,$uid,$gid,$rdev,$size,
$atime,$mtime,$ctime,$blksize,$blocks,$ft) = @stat;

  my $rfilename = $filename;
  $rfilename = $1 if($rfilename =~ /^$class->{'dir'}(.+)/);
  $rfilename =~ s/^\.?\/+//g;
  
  if(!$ft) {
    $ft  = "O";
    $ft  = "F" if(S_ISREG($mode));
    $ft  = "D" if(S_ISDIR($mode));
    $ft  = "L" if(($^O ne "MSWin32")&&(S_ISLNK($mode)));
  }
  
  my $rl;
  if($ft eq "L") {
    $rl = readlink($filename);
    $size = length($rl);
  }
  
  # ok, lets build the header
  my $rest = "$mtime|$atime|$ft|$mode|$uid|$gid|$rdev|$size|$rfilename";
  my $hdr = SZARBEGIN.SZARVERSION.length($rest)."|".$rest;
  
  if($ft eq "F") {
     my $x = open(my $i, "<$filename");
     if(!$x) { warn "cant open $filename for reading: $!"; return; }
     binmode $i;

     $class->_mywrite($hdr);	 
     my $hatravan = $size;	 
     my $full = 0;
     while($hatravan > 0) {
	  my $actread = ($hatravan > READ_BUFFER ? READ_BUFFER : $hatravan);
	  my $abuf;
	  my $ar = read $i, $abuf, $actread;
	  if(!$ar) { 
             warn "Read error while reading $filename: $!";
             last;
          }
          $full+= $ar;
	  $hatravan -= $ar;
	  
	  $class->_mywrite($abuf);	 

	  if($ar != $actread) {
             warn "Read error while reading $filename (we wanted more, file has probably changed while reading it)";
             last;
          }
     }
     close($i);
     if($full != $size) {
        # we need to pad it to the original size
        my $hatravan = $size - $full;
        while($hatravan > 0) {
	  my $actread = ($hatravan > READ_BUFFER ? READ_BUFFER : $hatravan);
          my $abuf = "\0" x $actread;
          $hatravan -= $actread;
	  $class->_mywrite($abuf);	 
        }

        # and then we should send a repair entry here
        $class->write_entry(
              $ofilename, $dev,$ino,$mode,$nlink,$uid,$gid,$rdev,$full,
              $atime,$mtime,$ctime,$blksize,$blocks,"R");
     }
  }
  elsif($ft eq "L") {
     $class->_mywrite($hdr);
     $class->_mywrite($rl);
  }
  elsif($ft eq "R") {
     $class->_mywrite($hdr);
  }
  else {
    $class->_mywrite($hdr);  
  }
  
  
  # ok, ready.
  return 1;
}
sub write_eos {
  my $class = shift;
  die "You didnt open szar for writing" if(!$class->{'wfh'});
  $class->_mywrite(SZAREND, 1);
}
sub myprint {
  my $obj = shift;
  my $msg = shift;
  return if(!$obj->{'debug'});
  warn "SZAR DEBUG: $msg";
}

sub read_entry {
  my $class = shift;

  die "You didnt open szar for reading" if(!$class->{'rfh'});
  die "Use extract or skip before you call read_entry" if($class->{'lastentry'});
  
  my $chunk;
  $class->myprint("Reading head");
  my $n = $class->_myread(\$chunk, HEAD);
  
  if(($n == length SZAREND) && ($chunk eq SZAREND)) {
     $class->myprint("Got end of stream ($class->{'compression'})");
     # clear end of stream. we still need to read the last bytes of the compression layer
     while($class->{'compression'} ne COMPRESS_NONE) {
       $class->myprint("Reading again for end of compression layer");
       $class->_myread(\$chunk, 0);
     }
     return;
  }
  die "Broken stream #1" if($n < HEAD);
  
  die "Invalid magic $chunk" if(SZARBEGIN ne substr($chunk, 0, length SZARBEGIN));
  die "Unsupported szar version $chunk" if(SZARVERSION ne substr($chunk, length SZARBEGIN, 2));
  my $tovabbi = 8;
  $class->myprint("Reading tovabbi");
  $n = $class->_myread(\$chunk, $tovabbi);
  die "Broken stream #2" if($n != $tovabbi);
  my $rest = $chunk;
  die "Cant parse beginning of the header $rest" if($rest !~ /^(\d+)\|/);
  my $hdrlength = $1;
  
  # lets calculate the rest of the header
  $n = $hdrlength - $tovabbi + length($hdrlength)+1;
  if($n > 0) {
    my $rbuf;
    $class->myprint("Reading rest");
    die "Cant read header fully" if($n != $class->_myread(\$rbuf, $n));
    $rest .= $rbuf;
  }
  die "Cant parse the header: $rest" if($rest !~ /^(\d+)\|(-?\d+)\|(-?\d+)\|(.)\|(\d+)\|(\d+)\|(\d+)\|(\d+)\|(\d+)\|(.+)$/);
  my %entry = (
    'hdrlngth'=> $1,
    'mtime'=> $2,
    'atime'=> $3,
    't'=> $4,
    'mode'=> $5,
    'uid'=> $6,
    'gid'=> $7,
    'rdev'=> $8,
    'filesize'=> $9,
    'filename'=> $10
  );
  
  die("Invalid filesize") if($entry{'filesize'} < 0);
  die("Invalid filesize for symlink") if(($entry{'filesize'} == 0)&&($entry{'t'} eq "L"));

  return $class->{'lastentry'} = \%entry;
}

# betomorites
sub _get_write_handle {
  my $obj = shift;

  if($obj->{'compression'} eq COMPRESS_GZIP) {
    $obj->{'wfh'} = new Compress::Raw::Zlib::Deflate('-Level'=>$obj->{'compression_level'},'AppendOutput'=>1 ) or die "Zlib error: cannot create deflation stream"
  }
  elsif($obj->{'compression'} eq COMPRESS_BZIP2) {
    my ($bz, $status) = new Compress::Raw::Bzip2(1) or
      die "Cannot create bzip2 deflate object";
    $obj->{'wfh'} = $bz;
  }
  else {
    $obj->{'wfh'} = 1;
  }
}

# kitomorites
sub _get_read_handle {
  my $obj = shift;

  if($obj->{'compression'} eq COMPRESS_GZIP) {
    $obj->{'rfh'} = new Compress::Raw::Zlib::Inflate( )  or die "Zlib error: cannot create inflation stream";
  }
  elsif($obj->{'compression'} eq COMPRESS_BZIP2) {
    my ($bz, $status) = new Compress::Raw::Bunzip2 or
      die "Cannot create bzip2 inflate object";
    $obj->{'rfh'} = $bz;
  }
  else {
    $obj->{'rfh'} = 1;
  }
}
sub _readable_or_die {
  my $class = shift;
  if($class->{'select'}) {
     my @r = $class->{'select'}->can_read($class->{'timeout'});
     die "Read timeout" if(!scalar @r) ;	 
  }
}
sub _writeable_or_die {
  my $class = shift;
  if($class->{'select'}) {
     my @r = $class->{'select'}->can_write($class->{'timeout'});
     die "Write timeout" if(!scalar @r) ;	 
  }
}

sub _myread {
  my $obj = shift;
  my $bufref = shift;
  my $amount = shift;
  my $lbuf;

  my $l = length($obj->{'lbuf'});
  $obj->myprint("_myread, wr $amount, bl: $l, $obj->{'compression'}");
  if(($l)&&($l >= $amount)) {
    goto MYRE;
  }

  my $ubuf = "";
UJRAOLVAS:
  my $uubuf;
  $obj->_readable_or_die();  
  my $n = read($obj->{'fh'}, $uubuf, READ_BUFFER);  
  if(!$n) {
    if(length($obj->{'lbuf'})) {
      print "not readable but cached data\n";
      $$bufref = $obj->{'lbuf'};
      $obj->{'lbuf'}="";
      return length($$bufref);
    }
    die "Read error: $!" ;
  }
  $ubuf .= $uubuf;
  $obj->{'read_bytes'} += $n;
  $obj->myprint("read $n bytes");

  my $abuf = "";
  my $r;
  if($obj->{'compression'} eq COMPRESS_GZIP) {
    $r = $obj->{'rfh'}->inflate($ubuf, $abuf);
    die "Zlib error on read: $r" if(($r != Z_OK)&&($r != Z_STREAM_END)&&($r ne ""));
    $obj->myprint("zlib decompression: $r");
    if ($r == Z_STREAM_END) {
       $obj->myprint("got end of zlib layer");
       $obj->{'compression'}= COMPRESS_NONE;
    }
  }
  elsif($obj->{'compression'} eq COMPRESS_BZIP2) {
    $r = $obj->{'rfh'}->bzinflate($ubuf, $abuf);
    die "Bzip2 error on read: $r" if(($r != BZ_OK)&&($r != BZ_STREAM_END)&&($r ne ""));
    $obj->myprint("bzip2 decompression $r");
    if ($r == BZ_STREAM_END) {
       $obj->myprint("got end of bzip2 layer");
       $obj->{'compression'} = COMPRESS_NONE;
    }
  } else {
    $r = "OK";
    $abuf = $ubuf;
  }


  $obj->myprint("uncompressed ".length($abuf));

  $obj->{'lbuf'} .= $abuf;

MYRE:
  $l = length($obj->{'lbuf'});
  if($amount > $l) {
    $ubuf = "";
    goto UJRAOLVAS;
  }
#  $obj->myprint("substr before");
  $$bufref = substr($obj->{'lbuf'},0, $amount);
  $obj->{'lbuf'} = substr($obj->{'lbuf'}, $amount);
#  $obj->myprint("substr end");

  return length($$bufref);
}
sub _mywrite {
  my $obj = shift;
  my $buf = shift;
  my $doflush = shift;

# eloszor a tomorites
  my $tosend;

  if($obj->{'compression'} eq COMPRESS_GZIP) {
    my $r = $obj->{'wfh'}->deflate($buf, $tosend);
    die "Zlib error on write: $r" if($r != Z_OK);
    if($doflush) {
        my $r = $obj->{'wfh'}->flush($tosend);
        die "Zlib error on close: $r" if($r != Z_OK);
    }
    
  }
  elsif($obj->{'compression'} eq COMPRESS_BZIP2) {
    my $r = $obj->{'wfh'}->bzdeflate($buf, $tosend);
    die "Bzip2 error on write: $r" if($r != BZ_RUN_OK);
    if($doflush) {
       my $r = $obj->{'wfh'}->bzclose($tosend);
       die "Bzip2 error on close: $r" if($r != BZ_STREAM_END);
    }
  } else {
    $tosend = $buf;
  }
  return if (!$tosend);

  $obj->_writeable_or_die();  
  my $fh = $obj->{'fh'};
  print $fh $tosend or die "Write error: $!";  
  $obj->{'sent_bytes'} += length($tosend);
}

1;
