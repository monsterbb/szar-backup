#!/usr/bin/perl

use warnings;
use strict;
use IO::Socket::SSL;
use IO::Select;
use Log::Log4perl qw(:easy);
use Data::Dumper;
use vars qw($dbh);
use FindBin qw($Bin);
use Cwd qw(realpath);
use File::stat;
use File::Path qw(make_path);
use Fcntl ':mode';
use POSIX ":sys_wait_h";
use Errno ':POSIX';
use DBI qw(:sql_types);
use DBD::SQLite;

# preloading stuffs because of the chroot
use Carp::Heavy; 
use Net::SSLeay;

BEGIN {
push @INC, "$Bin";
}
use DaemonizeChroot qw(daemonize);
use Archive::SzarStream;

###################################################################################x
require "$Bin/szar-backup-common.pl";

my $backuplock = ".backup.lock";
my $SERVER_VERSION = "1.0.1";
my $conffile = $ARGV[0];
if(!$conffile) {
  Usage();
  exit;
}
die "Cant find conf file" if((!$conffile)||(!-s $conffile));

$| = 1;

INFO("szar-Backup-server v$SERVER_VERSION");
my $conf = parseconf();
Log::Log4perl::init( \$conf->{'log4perl'} ) or die("Cant parse log conf");

die("At least DBD::SQLite 1.41_04 is needed, you have $DBD::SQLite::VERSION") if($DBD::SQLite::VERSION lt "1.41_04");




$SIG{__WARN__} = sub { WARN(@_); };
$SIG{__DIE__} = sub { FATAL(@_); };
$SIG{PIPE} = "IGNORE";
$SIG{CHLD} = \&REAPER;

our $dbh;

# lets setup the listening connection

my $serverSock;
if($conf->{'ssl'}) {
   $serverSock = IO::Socket::SSL->new( Listen => 5,
		   LocalAddr => $conf->{'localaddr'},
		   LocalPort => $conf->{'localport'},
		   Proto     => 'tcp',
		   Reuse     => 1,
		   SSL_ca_file => $conf->{'ssl.ca_file'},
		   SSL_key_file => $conf->{'ssl.key'},
		   SSL_cert_file => $conf->{'ssl.crt'},
		   SSL_verify_mode => 1+2+4, # verify peer + fail if no cert + verify client once
		   SSL_passwd_cb => sub {return $conf->{'ssl.passphrase'}},
		 ) or
    die "unable to create socket: ", &IO::Socket::SSL::errstr, " $@";
}else {
   $serverSock = IO::Socket::INET->new( Listen => 5,
		   LocalAddr => $conf->{'localaddr'},
		   LocalPort => $conf->{'localport'},
		   Proto     => 'tcp',
		   Reuse     => 1,
		 ) or
    die "unable to create socket: $@";
}
DEBUG "Server socket created";

if(!$conf->{'daemonize'}) {
  if($conf->{'chroot'}) {
    # require Unix::Mknod if($^O ne "MSWin32"); 
    chroot($conf->{'storage'}) or die("Cant chroot to $conf->{'storage'}: $!");
    chdir("/");
  }

  # set user anyway
  DaemonizeChroot::set_user($conf->{'user'}, $conf->{'group'}) if (!$conf->{'daemonize'});
} else {
  daemonize($conf->{'user'}, $conf->{'group'}, $conf->{'pidfile'}, $conf->{'chroot'}?$conf->{'storage'}:undef) ;
}
$conf->{'storage'} = "/" if($conf->{'chroot'});

die("cant write storage dir: $conf->{'storage'}") if((!-d $conf->{'storage'})||(!-w $conf->{'storage'}));
unlink($conf->{'storage'}."$backuplock");


INFO "szar-Backup server started";

my $sendworking = 0;
my $cp_lock_status = 0;
my ($sent_size,$read_size,$sessions, $clientSock, $clientSelect, $last_write) = (0, 0, 0);
my $subject_cn = "";
my $subrepo = "";
my $priv_storage_directory;
while (1) {
  DEBUG "waiting for next connection.";
  
  while($clientSock = $serverSock->accept()) {
    # at this point the client authentication has happened successfully
    my $pid;
    next if ($pid = fork);
    die "fork - $!\n" unless defined $pid;

    # we are the process dealing with the client
      close($serverSock);

      eval {
      handleclient();
      };
      ERROR $@ if($@);
      cp_unlock();
      close $clientSock;

      showstats();
      exit 0;
  }
}
$serverSock->close();
DEBUG "exiting";

sub init_storage_directory {

  my $strg = storage_directory();
}
sub storage_directory_subrepo_root{
   my $re = $conf->{"storage"};
   if($conf->{'certificate_common_name_mode'}) {
     die "Error, server is in certificate_common_name_mode and subject_cn is not configured" if(!$subject_cn);
     $re .= "/$subject_cn";
     if(!-d $re) {
        mkdir($re) or die "Cant create $re, $!";
     }
   }
   $re .= "/";
   $re =~ s#/{2,}#/#g;
   return $re;
}
sub storage_directory_full{
   my $re = storage_directory_subrepo_root();
   if($conf->{'subrepo_mode'}) {
      die "Error, server is in subrepo_mode and subrepo is not configured" if(!$subrepo);
      $re .= "/$subrepo";
      if(!-d $re) {
          mkdir($re) or die "Cant create $re, $!";
      }
   }
   $re .= "/";
   $re =~ s#/{2,}#/#g;
   return $re;
}
sub storage_directory{
   if(!$priv_storage_directory) {
      $priv_storage_directory = storage_directory_full();
      my $strg = $priv_storage_directory;
      for my $d ($strg, $strg."tmp") {
        if(!-d $d) {
          mkdir($d) or die "Cant create $d, $!";
        }
        die("$d is not writeable") if(!-w $d);
      }

   }

   return $priv_storage_directory;
}
sub cp_unlock {
  return if(!$cp_lock_status);
  $cp_lock_status = 0;
  unlink(storage_directory().$backuplock);
}
sub cp_locked {
  return $cp_lock_status;
}
sub cp_lock {
  return 0 if($cp_lock_status);
  return 0 if(-e storage_directory().$backuplock);
  mytouch(storage_directory().$backuplock);
  $cp_lock_status = 1;
}
sub showsize {
  my $b = shift;

  $b = sprintf("%d", $b);

  my $bs = "$b bytes";
  if($b > 1024*1024*1024) {
    $bs = sprintf("%.1f gbytes ($bs)", ($b / 1024 / 1024 / 1024));
  }
  elsif($b > 1024*1024) {
    $bs = sprintf("%.1f mbytes ($bs)", ($b / 1024 / 1024));
  }
  elsif($b > 1024) {
    $bs = sprintf("%.1f kbytes ($bs)", ($b / 1024));
  }

  return $bs;
}
sub showstats {
  return if(!$conf->{'showstats'});

  INFO "-------------------------------- Session";
  INFO "Read: ".showsize($read_size);
  INFO "Sent: ".showsize($sent_size);
}
sub REAPER {                            
    my $stiff;
    while (($stiff = waitpid(-1, WNOHANG)) > 0) {
	DEBUG ("child $stiff terminated -- status $?");
    }
    $SIG{CHLD} = \&REAPER;
}  
sub findlatestcp {
  my $full = lastcps();
  my @re;
  for (my $i = 1; $i < scalar @$full; $i+=2) {
    push @re, $full->[$i];
  }
  return \@re;
}
sub getdbpath {
  return storage_directory().$conf->{'name'}.".db";
}
my $lastsendworking;
sub proghndlr {
  return 0 if(!$sendworking);

  my $now = time();
  if($sendworking == 1) {
    $sendworking = 2;
    $lastsendworking = $now;
    return 0;
  }
  if($now - $lastsendworking >= 10) {
    $lastsendworking = $now;
    mywrite("WORKING", 1);
  }
  return 0;
}
sub closedb{
  return if(!$dbh);
  $dbh->disconnect();
  $dbh = undef;
}
sub initdb{
  return if ($dbh);

  my $dbf = getdbpath();
  my $dsn = "DBI:SQLite:dbname=$dbf";
  $dbh = DBI->connect($dsn, "", "") or die("cant open db");
  $dbh->{AutoCommit}=0;
  $dbh->{RaiseError}=1;
  $dbh->{sqlite_see_if_its_a_number}=1;
  $dbh->func(30, \&proghndlr, "progress_handler");

  if(!-s $dbf) {
    $dbh->do("CREATE TABLE checkpoint (id INTEGER PRIMARY KEY AUTOINCREMENT, ts CHARACTER(23) NOT NULL);");
    $dbh->do("CREATE TABLE files (
       id INTEGER PRIMARY KEY AUTOINCREMENT,
       checkpoint INTEGER NOT NULL,
       ocheckpoint INTEGER NOT NULL,
       rdev INT UNSIGNED NOT NULL,
       mtime INT UNSIGNED NOT NULL,
       atime INT UNSIGNED NOT NULL,
       t CHAR(1) NOT NULL,
       mode INT UNSIGNED NOT NULL,
       uid SMALLINT UNSIGNED NOT NULL,
       gid SMALLINT UNSIGNED NOT NULL,
       size INT UNSIGNED NOT NULL,
       dir TEXT,
       name TEXT
    );");
    $dbh->commit;

    INFO "DB tables successfully created";
  }

  if(!gc('select name from sqlite_master where type="index" and name="csfb"')) {
    INFO "Creating index...";
    $dbh->do("CREATE INDEX csfb ON files (checkpoint, dir, name)");
    $dbh->commit();
    INFO "Index created.";
  }




  for my $sql (@{$conf->{'dbinit'}}) {
    $dbh->do($sql);
  }

}

sub get_temp_file {
 return storage_directory().'tmp/'.int(rand(100000));

}

sub filetipusstr {
  my $fp = shift;
   my $t = "O";
   $t = "F" if(S_ISREG($fp->mode));
   $t = "D" if(S_ISDIR($fp->mode));
   $t = "L" if(S_ISLNK($fp->mode));

  return $t;
}
sub lastcps {
  my $limit = shift;
  my $limitstr = "";
  $limitstr = " LIMIT $limit " if($limit);

  my @re;
  my $sth = mqwo("SELECT id, ts FROM checkpoint WHERE ts != 'latest' ORDER BY ts DESC $limitstr");
  while(my $ref = $sth->fetchrow_hashref()) {
    my $ts = getcpfilename($ref->{'ts'});
    push @re, $ref->{'id'};
    push @re, $ts;
  }
  $sth->finish;
  return \@re;
}

sub cleanup_internal {

      my($deleteddirs, $deletedcps) = (0,0);
      for my $days (sort keys %{$conf->{'cleanup'}}) {
         # lekerdezzuk az x napnal regebbi checkpointokat
         my %cps;
         my $solds = mqwo("SELECT * FROM checkpoint WHERE ts < DATETIME('now','-$days DAY')  AND id != 0");
         while(my $ref = $solds->fetchrow_hashref()) {
            $cps{$ref->{'id'}} = $ref->{'ts'};
         }
         $solds->finish();

         next if(!scalar keys %cps);

         # megnezzuk hogy full torlesrol van e szo
         my $full = 0;
         for my $dr (@{$conf->{'cleanup'}->{$days}}) {
            if($dr eq "/") {
               $full = 1;
               last;
            }
         }

         if(!$full) {
            # csak nehan specifikus dirt torlunk
            my $sth = mqwo("SELECT DISTINCT dir, checkpoint FROM files WHERE checkpoint IN (".join(',',keys %cps).") ORDER BY LENGTH(dir) DESC");
            while (my $ref = $sth->fetchrow_hashref()) {
               next if($ref->{'dir'}eq "/");

               for my $dr (@{$conf->{'cleanup'}->{$days}}) {
                  if($ref->{'dir'}=~ /$dr/) {
                     # torlendo fajlok
                     INFO "DELETING ".$cps{$ref->{'checkpoint'}}." $ref->{'dir'} IN CLEANUP (older than $days days)";
                     DEBUG "MATCHING REGEXP: $dr";
                     $deleteddirs++;

                     my $srcdir = storage_directory().getcheckpointts($ref->{'checkpoint'}).$ref->{'dir'};
                     DEBUG "src: $srcdir";
                     my $dstdir = get_temp_file();
                     DEBUG "dst: $dstdir";

                     if($ref->{'dir'} =~ /(.+\/)(.+)\/$/) {
                       mqwo("DELETE FROM files WHERE checkpoint=? AND dir=? AND name=?", $ref->{'checkpoint'}, $1, $2);
                     }
                     mqwo("DELETE FROM files WHERE checkpoint=? AND dir LIKE ?", $ref->{'checkpoint'}, "$ref->{'dir'}%");

                     if(-d $srcdir) {
                       rename($srcdir, $dstdir) or die "Couldnt rename $srcdir: $!";
                     } else {
                       WARN "Cant find $srcdir in cleanup... it should exist";
                     }
                     $dbh->commit;
                     emptytmp(storage_directory().'tmp');
                  }
               }
            }
            $sth->finish();
         } else {
            for my $cp (sort values %cps) {
                $deletedcps++;
                INFO "DELETING CP FULLY IN CLEANUP: $cp (older than $days days)";
                delcp($cp, 1);
            }
         }
      }
      INFO "Deleted dirs ($subrepo): $deleteddirs, deleted cps: $deletedcps";

  return ($deleteddirs, $deletedcps);
}

sub handleclient {

      ($sendworking,$sent_size,$read_size,$sessions) = (0, 0, 0,0);

      if($conf->{'ssl'}) {
        my $subject_name = $clientSock->peer_certificate("subject");
        my $issuer_name = $clientSock->peer_certificate("issuer");
        $subject_cn = $clientSock->peer_certificate("cn");

        INFO "Client connection opened";
        INFO "Subject: '$subject_name'\n";
        INFO "CN: '$subject_cn'\n";
        INFO "Issuer: '$issuer_name'\n";

        die "Invalid characters in common name!" if($subject_cn !~ /^[\w\d_\-\. ]+$/);
        die "Invalid characters (..) in common name!" if($subject_cn =~ /\.{2,}/);
      }

  binmode $clientSock;
  $clientSock->autoflush();

  $clientSelect = IO::Select->new();
  $clientSelect->add(fileno($clientSock));

  my $authenticated = 0;

  mywrite("OK Welcome to szar-Backup v$SERVER_VERSION");

  while(1) {
   my $cmd = myread();
   return if(!$cmd); # if its undef...
   $cmd = trim($cmd);

   if($cmd =~ /^TEST$/) {
        mywrite("OK") ;
   }
   elsif($cmd =~ /^AUTH (.+)/) {
     my $cpw = $1;
     if(($conf->{password})&&($cpw eq $conf->{'password'})) {
        mywrite("OK") ;
        $authenticated = 1;
     } else {
        mywrite("NO") ;
     }
   }
   elsif($cmd =~ /^SUBREPO (.+)/) {
      die "server is not in subrepo mode" if(!$conf->{'subrepo_mode'});

        $subrepo = $1;
        die "Invalid characters in subrepo" if($subrepo !~ /^[\w\d\-_]+$/);
        mywrite("OK") ;
   }
   elsif($cmd =~ /^DELSTALLED$/) {
      if(!$conf->{'subrepo_mode'}) {
         mywrite("NO, THIS COMMAND WORKS ONLY IN SUBREPO MODE");
         next;
      }

      if(!$conf->{'stalled_days'}) {
         mywrite("NO, NOT CONFIGURED");
         next;
      }

      mywrite("OK, REMOVING STALLED REPOS IN THE BACKGROUND");
      disconnect();

      # a subrepo might have been sent even though this command is global
      closedb();

      my $root = storage_directory_subrepo_root();
      opendir(my $dh, $root) or die "cant open subrepo root: $!";
      while(my $d = readdir($dh)) {
         next if($d !~ /^[\w\d\-_]+$/);

         $subrepo = $d;
         INFO "Stale checking $subrepo";
         eval {
            initdb();

            my $sth = mqwo("SELECT julianday(datetime('now')) - julianday(max(ts)) AS x FROM checkpoint WHERE ts != 'latest'");
            $sth->execute();
            my $ref = $sth->fetchrow_hashref();
            $sth->finish();

            INFO "Stale checking $subrepo: ".($ref->{'x'}||"unknown");
            if(($ref->{'x'})&&($ref->{'x'} > $conf->{stalled_days})) {
               my $subrepo_path = "$root/$d";
               INFO "Removing subrepo as is ($subrepo_path)";
               emptytmp($subrepo_path);
               rmdir($subrepo_path);
            }

         };
         ERROR "Stale checking of $subrepo failed: $@" if($@);
         closedb();
      }
      closedir($dh);
      return;

   }
   elsif($cmd =~ /^CLEANUP$/) {

      if((!$conf->{'subrepo_mode'})||($subrepo)) {
         if(!cp_lock()) {
            mywrite("NO, CP LOCKED") ;
            next;
         }
      }

      mywrite("OK, CLEANING UP IN THE BACKGROUND");
      disconnect();

      my($deleteddirs, $deletedcps) = (0,0);
      if((!$conf->{'subrepo_mode'})||($subrepo)) {

         my($adeleteddirs, $adeletedcps) = cleanup_internal();
         $deleteddirs+= $adeleteddirs;
         $deletedcps+= $adeletedcps;

      } else {
         INFO "Cleanup in subrepo mode";
         my $root = storage_directory_subrepo_root();
         opendir(my $dh, $root) or die "cant open subrepo root: $!";
         while(my $d = readdir($dh)) {
            next if($d !~ /^[\w\d\-_]+$/);
            $subrepo = $d;
            if(!cp_lock()) {
                ERROR "Cant cleanup $subrepo right now, its locked";
                next;
            }
            INFO "Cleaning up $subrepo";
            eval {
               my($adeleteddirs, $adeletedcps) = cleanup_internal();
               $deleteddirs+= $adeleteddirs;
               $deletedcps+= $adeletedcps;
            };
            ERROR "Error while cleaning up $subrepo: $@" if($@);
            cp_unlock();
         }
         closedir($dh);
      }

      INFO "Deleted dirs: $deleteddirs, deleted cps: $deletedcps";
      return;
   }
   elsif($cmd =~ /^DELCP (\d{4})(\d\d)(\d\d)(\d\d)(\d\d)(\d\d)$/) {
      if(!cp_lock()) {
         mywrite("NO, CP LOCKED") ;
         next;
      }

     my $scp = "$1$2$3$4$5$6";
     my $scpf = "$1-$2-$3 $4:$5:$6";

     if(!$authenticated) {
        ERROR "Client tried to delete without authentication!";
        mywrite("NO, F U") ;
        next;
     }

     delcp($scpf);

     mywrite("OK") ;
   } 
   elsif($cmd =~ /^EXISTS (.+)$/) {
        my $w = $1;
        my ($rp,$fp) = validpath($w);
        if(($rp)&&($fp)) {
           my $t = filetipusstr($fp);
           mywrite("OK $t ".$fp->size) ;
        } else {
           mywrite("NO") ;
        }
   }
   elsif($cmd =~ /^CPLATEST (.+)/) {
      if($1 ne $conf->{'name'}) {
           mywrite("NO, YOU PROBABLY WANT TO CONNECT TO ANOTHER SERVER (I AM $conf->{'name'})");
           next;
      }

      my $x = findlatestcp();
      if(scalar @$x) {
           mywrite( "OK ".$x->[0]) ;
      }else {
           mywrite("OK $conf->{'name'}") ;
      }
   }
   elsif($cmd =~ /^CPLIST$/) {
      my $x = findlatestcp();
      if(scalar @$x) {
           mywrite( "OK ".join(' ', @$x)) ;         
      }else {
           mywrite("NO") ;
      }
   }
   elsif($cmd =~ /^(CPNEXT|CPNEW)$/) {
      if(!cp_lock()) {
         mywrite("NO, CP LOCKED") ;
         next;
      }

      my $lastcp = gcaa("SELECT * FROM checkpoint ORDER BY ts DESC LIMIT 1");
      if(!$lastcp) {
        mqwo("INSERT INTO checkpoint (id,ts) VALUES (0,'latest') ");
        mkdir(storage_directory().'latest') or die("couldnt create latest dir: $!");
      }
      mqwo("INSERT INTO checkpoint(ts) VALUES (DATETIME('now')) "); # dont create dir for this one... we might never use it for storing.
      if(($lastcp)&&($cmd eq "CPNEW")) {
        mqwo("UPDATE files SET checkpoint=? WHERE checkpoint=0", $lastcp->{'id'});
        my $cpf = getcpfilename($lastcp->{'ts'});
        rename(storage_directory().'latest', storage_directory().$cpf) or die("couldnt rename latest to $cpf");
        eval { $dbh->commit(); } ;
        if($@) {
           # roll back directory... strange shit, but its unfortunately needed, since commit might drop an exception
           rename( storage_directory().$cpf, storage_directory().'latest') or die("couldnt rename $cpf to latest");

           mywrite("NO");
           return;
        }
      }else {
        $dbh->commit();
      }

     mywrite("OK") ;
   }
   elsif($cmd =~ /^CPEND/) {
     cp_unlock();
     mywrite("OK") ;
     return;
   }
   elsif($cmd =~ /^MOVE$/) {
      if(!cp_locked()) {
         mywrite("NO, CP UNLOCKED") ;
         next;
      }

     my ($latestcpid,$latestcp,$oldcpid, $oldcp) = @{lastcps(2)};
     if(!$oldcp) {
       WARN "NO old cp";
       mywrite("NO") ;
       next;
     }
     mywrite("OK GO AHEAD") ;

      my $newcpid = 0;
      my $newcp = "latest";
      my $db = 0;
      my @files;
      while(my $f = myread()) {
         last if($f =~ /^READY/);
         while($f =~ /^(\d+)\s*$/g) {
            my $fid = $1;
            my $physmove = $2;

            if(time() - $last_write > 10) {
               mywrite("WORKING", 1);
            }

            my $ref = gcaa("SELECT * FROM files WHERE id=? AND checkpoint=? AND ocheckpoint!=?", $fid, $newcpid, $latestcpid);
            if(!$ref->{'id'}) {
               WARN "Client wanted to move a file, it does not exist in db: $fid";
               next;
            }

            DEBUG "Moving files inside db begin";
            $sendworking = 1;
            my @hatra = ($ref);
            my @move;
            while(my $aref = shift @hatra) {
              proghndlr();
              push @move, $aref->{'id'};
              DEBUG "Mover: $aref->{'t'} ($aref->{'id'}) $aref->{dir}$aref->{'name'}";
              if($aref->{'t'} eq "D") {
                DEBUG "Directory, fetching subentries: $aref->{'dir'}$aref->{'name'}/%";
                my $asth = mqwo("SELECT * FROM files WHERE checkpoint=? AND dir=?", $newcpid, "$aref->{'dir'}$aref->{'name'}/");
                while (my $aaa = $asth->fetchrow_hashref()) {
                   proghndlr();
                   push @hatra, $aaa;
                }
                $asth->finish;
              }
            }
            my @ar;
            while(1) {
              proghndlr();
              if((scalar @move > 0) && (scalar @ar < 100)) {
                 push @ar, (shift @move);
              }
              elsif(scalar @ar) {
                 my $ids = join(",",@ar);
                 DEBUG "UPDATE files SET checkpoint=$oldcpid WHERE id IN ($ids)";
                 mqwo("UPDATE files SET checkpoint=? WHERE id IN ($ids)", $oldcpid);
                 @ar = ();
              }
              else {
                 last;
              }
            }
            $sendworking = 0;
            DEBUG "Moving files inside db end";

              my $fn = $ref->{'dir'}.$ref->{'name'};
              if(!-e storage_directory().$newcp.$fn) {
                 ERROR "Client wanted to move a file, it does not exist on disk: $fid $fn" if($ref->{'t'} ne "O");
              } else {
                my $bp = $ref->{'dir'};

                eval {
                  make_path(storage_directory().$oldcp.$bp);
                };
                my $dsrc = storage_directory().$newcp.$fn;
                my $ddst = storage_directory().$oldcp.$fn;

                DEBUG "Moving $dsrc -> $ddst";
                if(!rename($dsrc, $ddst)) {
                  WARN "Renaming file failed: $dsrc $ddst";
                  $dbh->rollback;
                  next;
                }
              }

            DEBUG "Commiting move in db";
            $dbh->commit();

            $db++;
         }
      }

      mywrite( "OK $db FILES MOVED") ;
   }
   elsif($cmd =~ /^PHANTOM$/) {
      if(!cp_locked()) {
         mywrite("NO, CP UNLOCKED") ;
         next;
      }

     my ($latestcpid,$latestcp,$oldcpid, $oldcp) = @{lastcps(2)};
     if(!$oldcp) {
       WARN "NO old cp";
       mywrite("NO") ;
       next;
     }
     mywrite("OK GO AHEAD") ;

      my $newcpid = 0;
      my $newcp = "latest";
      my $db = 0;
      my @files;
      while(my $f = myread()) {
         last if($f =~ /^READY/);
         while($f =~ /^(\d+) (\d+) (\d+) (\d+) (\d+) (\d+) (\d+)/g) {
            my ($fid, $mtime, $atime, $mode, $uid, $gid, $size) = ($1,$2,$3,$4,$5,$6, $7);

            my $ref = gcaa("SELECT * FROM files WHERE id=? AND t='D' AND checkpoint=? AND ocheckpoint!=?", $fid, $newcpid, $latestcpid);
            if(!$ref->{'id'}) {
               WARN "Client wanted to phantom a file, it does not exist in db: $fid";
               next;
            }

              # a regit atemeljuk
              mqwo("UPDATE files SET t='P',checkpoint=? WHERE id=?",$oldcpid, $ref->{'id'});
              # beszurunk egy ujat az uj parameterekkel
              my @params = (
                 $newcpid, $ref->{'ocheckpoint'}, $ref->{'rdev'},
                 $mtime, $atime, "D", 
                 $mode, 
                 $uid, $gid, 
                 $size, 
                 $ref->{'dir'},
                 $ref->{'name'} 
              );
              mqwostr("INSERT INTO files (checkpoint,ocheckpoint,rdev,mtime,atime,t,mode,uid,gid,size,dir,name) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)",
                 {(scalar @params) => SQL_VARCHAR},
                 @params
              );
              $db++;
         }
      }
      $dbh->commit() if($db);

      mywrite( "OK $db PHANTOM ENTRIES CREATED") ;
   }
   elsif($cmd =~ /^DB (SZAR|GZ|BZIP2)$/) {
      my $method = $1;
      my $fn = getdbpath();
      if(!-s $fn) {
         WARN "WTF NO DB?!!";
         mywrite( "NO") ;
         next;
      }
      mywrite( "OK") ;

      myget( $method, [$conf->{'name'}.".db"] );
   }
   elsif($cmd =~ /^PUT /) {
      if(!-e storage_directory().$backuplock) {
         mywrite("NO, CP UNLOCKED") ;
         next;
      }
      if($cmd !~ /^PUT (SZAR|GZ|BZ2)$/) {
         ERROR("Client wanted unknown PUT stuff");
         mywrite("NO, UNKNOWN METHOD") ;
         next;
      } 
      my ($method, $expectedfiles) = ($1,$2);
      my $cps = lastcps(1);
      my ($cpid, $cp) = ($cps->[0],$cps->[1]);
      if(!$cp) {
         WARN "NO cp";
         mywrite( "NO") ;
         next;
      }

      mywrite( "OK" ) ;

      myput( $method, $cpid );
   }
   elsif($cmd =~ /^GET /) {
      if(cp_locked()) {
         mywrite("NO, CP LOCKED") ;
         next;
      }
      if($cmd !~ /^GET (SZAR|GZ|BZ2) (\d+)$/) {
         ERROR("Client wanted unknown GET stuff");
         mywrite("NO, UNKNOWN METHOD") ;
         next;
      } 
      my ($method, $expectedfiles) = ($1,$2);

      mywrite("OK GO AHEAD") ;

      my @files;
      while(my $f = myread()) {
         last if($f =~ /^READY/);
         while($f =~ /^(\d+)\s*$/g) {
           my $fid = $1;
           my $ref = gcaa("SELECT * FROM files WHERE id=?",$fid);
           if($ref) {
             $ref->{'ts'} = getcheckpointts($ref->{'checkpoint'});
             $ref->{'fn'} = $ref->{'ts'}.$ref->{'dir'}.$ref->{'name'};
             if(($ref->{'t'} eq "F")&&(!-e storage_directory().$ref->{'fn'})) {
               ERROR "Client wanted to GET a file which doesnt exists on disk: $ref->{'fn'}";
               next;
             }
             push @files, $ref;
           } else {
             ERROR "Client wanted to GET a file which doesnt exists in db: $fid";
           }
         }
      }

      if(scalar @files != $expectedfiles) {
         ERROR("Client sent incorrect number of filenames (".scalar @files." != $expectedfiles)");
         mywrite("NO");
         next;
      }
      if(!scalar @files) {
        mywrite("NO, NO FILES");
        next;
      }
      mywrite( "OK ".(scalar @files)." FILES") ;

      myget( $method,  \@files );
   }
   else {
     WARN "Client sent unknown command: $cmd";
     mywrite( "NO") ;
   }
  }
}
my %cptscache;
sub getcheckpointts {
  my $cpid = shift; 
  if(!$cptscache{$cpid}) {
    $cptscache{$cpid} = gc("SELECT ts FROM checkpoint WHERE id=?",$cpid) ;
    $cptscache{$cpid} =~ s/(\-| |:|\.)//g;
  }
  return $cptscache{$cpid};
}

sub delcp {
  my $scpf = shift;
  my $skipclient = shift;
  my $rnd;
     my $cpid = gc("SELECT id FROM checkpoint WHERE ts=?", $scpf);
     if($cpid) {

       my $scp = getcheckpointts($cpid);

       mqwo("DELETE FROM checkpoint WHERE id=?",$cpid);
       mqwo("DELETE FROM files WHERE checkpoint=?", $cpid);
       if(-d storage_directory().$scp) {
         $rnd = int(rand(1000000))+1;
         rename(storage_directory().$scp, storage_directory()."tmp/$rnd") or die( "couldnt rename $scpf: $!" );
       }
       $dbh->commit;
       INFO "Checkpoint $scpf deleted from DB";
     } else {
        ERROR "Client wanted to delete an unexistent CP";
     }

     if($rnd) {
       emptytmp(storage_directory()."tmp", $skipclient?undef:time());
       INFO "Checkpoint $scpf deleted from disk";
     } 

}

sub myput {
  my ( $method, $ocpid ) = @_;

  my $destdir = storage_directory().'latest';
  my $cpid = 0; # this means: latest.
  my $files = 0;
  my $db = 0;
  eval {
    my $lshit = time();
    my $szar = Archive::SzarStream->read_handle($clientSock, {timeout=>$conf->{'timeout'},dir=>$destdir, compression=>$method, debug=>$conf->{'szar_debug'}});
    while(my $e = $szar->read_entry()) {
        my $now = time();
        if($now - $lshit > 10) {
            $lshit = $now;
            mywrite("WORKING", 1);
        }
        $files++;

        my($dir, $name) = ("/", $e->{'filename'});
        if($e->{'filename'} =~ /^(.*\/)(.+)$/) {
          $dir = "/$1";
          $name = $2;
        }

        my $fullfn = $destdir.'/'.$e->{'filename'};
        my $doextract = 1;
        DEBUG "Extracting $e->{'t'} $e->{'filename'} ($e->{'filesize'})";
        if(-l $destdir.'/'.$e->{'filename'}) {

           # this is added here for self healing
           my $err = 1;
           if($e->{'t'} eq "L") {
              $doextract = 0;
              my $dst = get_temp_file();
              if(!$szar->extract($dst)) {
                 ERROR "Could not extract symbolic link: $e->{'filename'}";
                 next;
              }

              my $olink = readlink($destdir.'/'.$e->{'filename'});
              my $nlink = readlink($dst);
              $err = 0 if($nlink eq $olink);
              INFO "Symbolic link reupload issue: $e->{'filename'} | $olink | $nlink | $err";
              unlink($dst);
           }

           if($err) {
             ERROR "Client wanted to upload to a symlink: $e->{'filename'}";
             $szar->skip() if($doextract);
             next;
           }
        }
        elsif(-d $fullfn) {
          # directories might already exist. thats ok.
          $szar->skip();
          $doextract = 0;
        }
        elsif(-e $fullfn) {

           my $ref = gcaa("SELECT * FROM files WHERE checkpoint=? AND dir=? AND name=?", $cpid, $dir, $name);
           if((!$ref)||(!$ref->{'id'})) {
             INFO "Orphan file found: $e->{'filename'}, allowing overwrite";
             unlink($fullfn);
           } else {

             ERROR "Client wanted to upload a file ($e->{'filename'}) which already exists?! Bug or hack attempt.";
             $szar->skip();
             next;
           }
        }

        if(($doextract)&&(!$szar->extract())) {
           if($e->{'t'} ne "O") {
             ERROR "Extracting $e->{'filename'} failed";
             next;
           }
        }


#        DEBUG "Extracting $e->{'t'} $e->{'filename'} ($e->{'filesize'}) => $dir XX $name";

        if($e->{'t'} eq "R") {
          # this is a repair entry, we should change filesize instead of insertint a new element
          DEBUG "Processing repair entry";
          mqwo("UPDATE files SET size=? WHERE checkpoint=? AND dir=? AND name=?", $e->{'filesize'}, $cpid, $dir, $name);
        } else {

           my @params = (
                 $cpid, 
                 $ocpid, 
                 $e->{'rdev'},
                 $e->{'mtime'}, 
                 $e->{'atime'}, 
                 $e->{'t'}, 
                 $e->{'mode'}, 
                 $e->{'uid'}, 
                 $e->{'gid'}, 
                 $e->{'filesize'}, 
                 $dir,
                 $name 
          );
          mqwostr("INSERT INTO files (checkpoint,ocheckpoint,rdev,mtime,atime,t,mode,uid,gid,size,dir,name) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)",
              {(scalar @params)=>SQL_VARCHAR},
              @params
              );
          $db++;
        }
    }
    DEBUG "Extraction ready";

    $read_size += $szar->{'read_bytes'};
  };
  if($@) {
      my $x = $@;
      $dbh->commit if($db);
      die $x;
  }

  $dbh->commit if($db);

  mywrite("OK $db FILES EXTRACTED OUT OF $files");

  return $db;

}
sub disconnect{
  $clientSelect->remove(fileno($clientSock));
  close $clientSock;
}

sub myget {
  my ($method, $u) = @_;

  return if(!scalar @$u);

  my $written = 0;
  eval {
     my $szar = Archive::SzarStream->write_handle($clientSock, {timeout=>$conf->{'timeout'},dir=>storage_directory(), compression=>$method,debug=>$conf->{'szar_debug'}, compression_level=>$conf->{'compression_level'}});
     for my $f (@$u) {
        my @p;
        my $fn = $f;
        if(ref($f) eq "HASH") {
           $fn = $f->{'fn'};
           push @p, $fn, 0, 0, $f->{'mode'}, 0, $f->{'uid'}, $f->{'gid'},$f->{'rdev'},$f->{'size'},
              $f->{'atime'},$f->{'mtime'},0, 0, 0, $f->{'t'};
        } else {
           push @p, $fn;
        }
        DEBUG "Sending $fn";
        if(!$szar->write_entry(@p)) {
          ERROR "Couldnt sent $fn";
        } else {
          $written++;
        }
     }
     $szar->write_eos();
     DEBUG "Sending ready";

    $sent_size += $szar->{'sent_bytes'};
  };
  die $@ if ( $@ );

  mywrite("OK $written FILES SENT");
  return $written;
}
sub validpath {
  my $w = shift;
  my $p = storage_directory().$w;

  $p = realpath($p);
  if(!$p) {
      ERROR "Client wanted an unexistent path: $w";
      return;
  }

  my $strg = storage_directory();
  if($p !~ /^$strg/) {
    ERROR("Client wanted a tricky path: $w -> $p");
    return;
  }

  my $st = stat($p);
  if(!$st) { WARN "Cant stat file $p: $!"; return; }

  return($p, $st);
}
sub mywrite {
  my ($msg,$nocheck) = @_;

  if(!$nocheck) {
    my @r =$clientSelect->can_read(0);
    if(scalar @r) {
      my $b;
      my $n = read($clientSock, $b, $conf->{'read_buffer_size'});
      die "client is disconnected or protocol is out of sync: $n $b";
    }
  }

  my @ready =$clientSelect->can_write($conf->{'timeout'});
  if(!scalar @ready) {
    die("Client is not writeable within timeout");
  }
  print $clientSock "$msg\r\n" or die "Write error: $! $SSL_ERROR" ;
  $last_write = time();
  DEBUG ">> $msg";
  $sent_size += length($msg)+2;
}
sub myread {

  my @ready = $clientSelect->can_read($conf->{'timeout'});
  if(!scalar @ready) {
    die("Client is not readable within timeout");
  }

      my $abuf = "";
      my $n = read( $clientSock, $abuf, $conf->{'read_buffer_size'});
      if ( !defined($n)) {
        die( "Read error: $? $! $SSL_ERROR" );
      } elsif ( $n == 0 ) {
        WARN( "Connection closed" );
        return ;
      }
      $read_size += $n;

      DEBUG "<< ".trim($abuf);
      return $abuf;
}
sub parseconf {
  my %conf;
  my @required = ("name","localaddr","localport","storage","ssl.key","ssl.crt","ssl.ca_file","pidfile",);
  my $tar = trim(`which tar`);
  my %optional = (
     "db_fetch_method"=>"GZ",
     "certificate_common_name_mode"=>0,
     "subrepo_mode"=>0,
     "szar_debug"=>0,
     "password"=>"",
     "ssl"=>1,
     "stalled_days"=>180,
     "compression_level"=>9,
     "daemonize"=>0,
     "user"=>"nobody",
     "group"=>"nogroup",
     "timeout"=>30,
     "showstats"=>1,
     "ssl.passphrase"=>"",
     "chroot"=>0,
     "read_buffer_size"=> 32768,
  );
  my @stuffs = (@required, keys %optional);

  my $crestr = "^(".join('|',@stuffs).")\\s*=\\s*(.+)";

  my @dbinitsqls;
  open(my $x, "<$conffile") or die("Cant open conf file");
  while(<$x>) {
    next if(/^\s*$/);
    next if(/^#/);
    if(/^log4perl/) {
       $conf{'log4perl'} .= $_;
       next;
    }

    # kulonben harapunk
    if(/$crestr/o) {
       $conf{$1} = $2
    }
    elsif(/^cleanup\s*(\d+)\s*=\s*(.+)/) {
       my ($days, $dirregexp) = ($1, $2);
       $dirregexp = qr/$dirregexp/ if($dirregexp ne "/");
       push @{$conf{"cleanup"}{$days}}, $dirregexp;
    }
    elsif(/^dbinit\s*=\s*(.+)/) {
       push @dbinitsqls, $1;
    } else {
      die("Invalid line in the config file: $_");
    }
  }
  close($x);

  $conf{'dbinit'} = \@dbinitsqls;

  for my $k (@required) {
    die("No value for $k in conf file!") if(!defined($conf{$k}));
  }

  for my $k (keys %optional) {
    $conf{$k} = $optional{$k} if(!defined($conf{$k}));
  }

  $conf{'storage'} =~ s/\/*$//g;
  $conf{'storage'}.='/';

  die "ssl must be enabled in certificate_common_name_mode" if((!$conf->{'ssl'})&&($conf->{'certificate_common_name_mode'}));

#  print Dumper(\%conf); exit;
  return \%conf;
}

sub Usage {
  die("Usage: $0 config_file");
}

sub emptytmp {
  my $dir = shift;
  my $otime = shift;
  my $db = shift || 0;

  opendir(my $X, $dir);
  while(my $f = readdir($X)) {
    next if($f =~ /^\.\.?$/);

      if($otime) {
         my $atime = time();
         if($atime - $otime > 10) {
           $otime = $atime;
           mywrite("PROGRESS $db");
         }
      }

    if(-l "$dir/$f") {
      unlink("$dir/$f") or WARN "Could not unlink $dir/$f: $!";
    }
    if(-d "$dir/$f") {
      $db = emptytmp("$dir/$f", $otime, $db);
      rmdir("$dir/$f") or WARN "Could not rmdir $dir/$f: $!";
    } else {
      unlink("$dir/$f") or WARN "Could not unlink $dir/$f: $!";
    }
  }
  closedir($X);

  return $db;
}
