#!/usr/bin/perl

package MyWebServer;

use warnings;
use strict;
use IO::Socket::SSL;
use IO::Select;
use Data::Dumper;
use vars qw($dbh);
use FindBin qw($Bin);
use DBD::SQLite;
use Net::Server;
use MIME::Base64;
use base qw(Net::Server::HTTP);
use IO::Zlib;
use Archive::Tar::Stream;
use Digest::HMAC_SHA1 qw(hmac_sha1_hex);

###################################################################################x
require "$Bin/szar-backup-common.pl";

my $WEB_VERSION = "1.0.1";
my $conffile = $ARGV[0];
if(!$conffile) {
  Usage();
  exit;
}
die "Cant find conf file" if((!$conffile)||(!-s $conffile));

$| = 1;

print("szar-Backup-web v$WEB_VERSION\n");
my $conf = parseconf();

$SIG{PIPE} = "IGNORE";


my $auth_token_to_be_appended;

 __PACKAGE__->run(
        host  => $conf->{'localaddr'},
        port  => $conf->{'localport'}.($conf->{'ssl'}?"/ssl":""),
        ipv => 4,
        reverse_lookups => 0,
        user=> $conf->{'user'},
        group=> $conf->{'group'},
        chroot => $conf->{'chroot'} || undef,
        setsid => $conf->{'daemonize'} || undef,
        log_level => $conf->{'log.level'},
        log_file => $conf->{'log.file'},
        access_log_file => $conf->{'log.access_log'},
#         no_client_stdout => 1,
        SSL_key_file  => $conf->{'ssl.key'},
        SSL_cert_file => $conf->{'ssl.crt'},
        SSL_ca_file => $conf->{'ssl.ca_file'},
        SSL_verify_mode => $conf->{'ssl.ca_file'} ? 1+2+4 : 0, # verify peer + fail if no cert + verify client once
        SSL_passwd_cb => sub {return $conf->{'ssl.passphrase'}},
        app =>{
            '/' => \&main,
        }
    );


sub Usage {
  die("Usage: $0 config_file");
}

sub parseconf {
  my %conf;
  my @required = ("localaddr","localport","pidfile");
  my %optional = (
     "log.level" => 0,
     "log.file" => undef,
     "log.access_log"=>undef,
     "ssl"=>1,
     "ssl.key"=>"",
     "ssl.crt"=>"",
     "ssl.ca_file"=>"",
     "daemonize"=>0,
     "user"=>"nobody",
     "group"=>"nogroup",
     "ssl.passphrase"=>"",
     "chroot"=>0,
     "auth.hmac"=>"",
     "auth.hmac_validity_seconds"=>86400,
     "auth.username"=>"",
     "auth.password"=>"",
     "hide_symlinks"=>0,
     "subrepo_mode"=>0,
  );
  my @stuffs = (@required, keys %optional);

  my $crestr = "^(".join('|',@stuffs).")\\s*=\\s*(.+)";

  my @repos;
  open(my $x, "<$conffile") or die("Cant open conf file");
  while(<$x>) {
    next if(/^\s*$/);
    next if(/^#/);

    # kulonben harapunk
    if(/$crestr/o) {
       my $k = $1;
       my $v = trim($2);
#print "parsed k $k v $v x\n";
       $conf{$k} = $v if(defined($v));
    }
    elsif(/^directory_restriction\s*=\s*(.+)/) {
       my $dirregexp = $1;
       $conf{'dirregexp'} = qr/$dirregexp/ if($dirregexp ne "/");
    }
    elsif(/^repo\s*=\s*(.+)/) {
       push @repos, $1;

    } else {
      die("Invalid line in the config file: $_");
    }
  }
  close($x);

  $conf{'repos'} = \@repos;

  for my $k (@required) {
    die("No value for $k in conf file!") if(!defined($conf{$k}));
  }

  for my $k (keys %optional) {
    $conf{$k} = $optional{$k} if(!defined($conf{$k}));
  }

  die "Authentication must be configured (either username/password or hmac based)" if((!$conf{'auth.hmac'})&&(!$conf{'auth.username'}));


#  print Dumper(\%conf); exit;
  return \%conf;
}


sub commandline {
    return "";
}

sub authenticated {
#die "here: ".$conf->{'auth.username'}." S";
  if(($conf->{'auth.username'})&&($conf->{'auth.username'} ne "-")) {
    my ($un, $pw) = getcredentials();
    if((!$un)||($un ne $conf->{'auth.username'})||(!$pw)||($pw ne $conf->{'auth.password'})) {
      authrequired("Auth needed");
      return 0;
    }
  }

  return 1;
}

my $repo;
my $repodir;
sub main {

  return unless(authenticated());

  my $params =  parse_query($ENV{'QUERY_STRING'});



  my $repos = validaterepos();
  die "No valid repos" if(!scalar keys %$repos);

  $repo = $params->{'r'}||"";
  $repodir = $repos->{$repo};
  if(!$repodir) {
     die "Invalid repo" if($repo);

     return redirect("/?r=".firstkey($repos)) if (scalar keys %$repos == 1);
  }



  my $cps_by_id = {};
  my $cps_by_ts = {};
  my $latestcpid;
  my $cpid = $params->{'c'}||"";

  if($conf->{'auth.hmac'}) {
     die "missing auth token" if(!$params->{'a'});
     my ($ats, $ahmac) = split('-', $params->{'a'});
     die "invalid auth token (no ts)" if(!$ats);
     die "invalid auth token (no hmac)" if(!$ahmac);
     die "token too old" if(time() - $ats > $conf->{'auth.hmac_validity_seconds'});
     my $token_should_be = hmac_sha1_hex("$repo|$ats", $conf->{'auth.hmac'});
     die "invalid token (not matching)" if($token_should_be ne $ahmac);

     # great success!
     $auth_token_to_be_appended = $params->{'a'};
  }

  if($repo) {
     ($cps_by_ts, $cps_by_id, $latestcpid) = lastcps();
     die "Invalid db, no cps" if(!scalar keys %$cps_by_ts);

     if(!defined($cps_by_id->{$cpid})) {
        die "Invalid checkpoint" if($cpid);

        my $d = $params->{'d'} || "";
        return redirect("/?r=$repo&c=$latestcpid&d=".$d);
     }
  }


  my $curdir = $params->{'d'};
  my $ocurdir = $curdir;
  $curdir = "/" if(!$curdir);
  $curdir =~ s#//#/#g;
  $curdir .="/" if($curdir !~ m#/$#);

  return redirect("/?r=$repo&c=$cpid&d=$curdir") if($ocurdir ne $curdir);

  if($conf->{'dirregexp'}) {
     # there is a directory restriction here

     die "directory restriction, security abuse is reported" if($curdir !~ $conf->{'dirregexp'});
  }


  if($params->{'leech'}) {
     my $file = gcaa("SELECT * FROM files WHERE id=?", $params->{'leech'});
     die "Invalid file specified" if((!$file->{'id'})||($file->{'t'} ne "F"));

     die "symlinks not supported" if($file->{'t'} eq "L");

     if($conf->{'dirregexp'}) {
       # there is a directory restriction here
       die "file restriction, security abuse is reported ".Dumper($file) if($file->{'dir'} !~ $conf->{'dirregexp'});
     }

     my $fullpath = getfullpath($file);

     open (my $x, "<$fullpath") or die "Couldnt open file";

     print "Content-type: application/octet-stream\r\nContent-Disposition: attachment; filename=$file->{'name'}\r\n\r\n";
     while(read($x, my $buf, 65536)) {
        print $buf;
     }
     close($x);
     return;
  }


  if($params->{'tar'}) {
     die "No checkpoint selected" if(!$cpid);

     my $dcurdir = $curdir;
     $dcurdir =~ s#/+$##g;
     $dcurdir =~ s#^/+##g;
     $dcurdir =~ s#/#_#g;
     my $dfilename = "${repo}_$cps_by_id->{$cpid}_$dcurdir.tar.gz";

     print "Content-type: application/x-gzip; charset=binary\r\nContent-Disposition: attachment; filename=$dfilename\r\n\r\n";

     my $fh = IO::Zlib->new("-","wb");

     my $ts = Archive::Tar::Stream->new(outfh => $fh);

     my $res = getdiritems($cpid, $latestcpid, $curdir, "ORDER BY dir", 1);
     while(my $ref = $res->fetchrow_hashref()) {

         next if($ref->{'t'} eq "P");

         my $afname = substr "$ref->{'dir'}$ref->{'name'}", 1;
         $afname = substr $afname, ((length $curdir) -1) if(($params->{'shortpath'})||($conf->{'dirregexp'}));

         my %extra;
         $extra{'mode'} = $ref->{'mode'};# oct()
         $extra{'uid'} = $extra{'uname'} = $ref->{'uid'};
         $extra{'gid'} = $extra{'gname'} = $ref->{'gid'};
         $extra{'mtime'} = $ref->{'mtime'};

         if($ref->{'t'} eq "D") {
            $extra{'typeflag'} = 5;
            $ts->AddFile($afname,0, undef, %extra);
         }
         elsif($ref->{'t'} eq "L") {
            next if($conf->{'hide_symlinks'});

            $extra{'typeflag'} = 2;
            my $pointsto =  readlink(getfullpath($ref));
            $ts->AddLink($afname,$pointsto, %extra);
         }
         elsif($ref->{'t'} eq "F") {
            my $fp = getfullpath($ref);
            open my $i, "<$fp" ;
            if($i) {
              $ts->AddFile($afname,$ref->{'size'}, $i, %extra);
              close($i);
            } else {
              $extra{'typeflag'} = 2;
              my $pointsto =  "/ERROR/FILE/NOT/FOUND/IN/BACKUP";
              $ts->AddLink($afname,$pointsto, %extra);
            }

         } else {
            die "Adding type $ref->{'t'} is not yet implemented";
         }


     }

     $ts->FinishTar();

     undef $ts;
     undef $fh;

     return;
  }



  print "Content-type: text/html\r\n\r\n"; 

  print <<END;
<!doctype html>
<html>
<head>
<meta charset="utf-8" />
<title>szar-backup web $WEB_VERSION</title>
<!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script> -->
<style TYPE="text/css"> 
<!--
body {
  margin: 10px;
}
#files {
  font-family:monospace;
  text-align: left;
}
tr.td {
  font-weight: bold;
}
#files tr td:nth-child(2),#files tr td:nth-child(3),#files tr td:nth-child(4),#files tr td:nth-child(5) {
  text-align: right;
}
#files tr td:nth-child(6) {
  width: 200px;
}
-->
</style>
</head>
<body>
END


  if($params->{'commitlog'}) {

  print "<table id='files'><thead><tr>
     <th>Checkpoint</th>
     <th>T</th>
     <th>Permissions</th>
     <th>Owner</th>
     <th>Group</th>
     <th>Filesize</th>
     <th>Mtime</th>
     <th>Filename</th></tr></thead>\n";
  print "<tbody>";

  my $dirstr = "$curdir%";


  my $res = mqwo("select files.*, checkpoint.ts from files left join checkpoint on checkpoint.id=files.checkpoint where dir like ? and checkpoint != 0 and t='F' and size > 0 ORDER BY checkpoint.ts DESC",
     $dirstr);

    while (my $ref = $res->fetchrow_hashref()) {

       my $mtime = localtime $ref->{'mtime'};
       my $mode =  sprintf "%06o", $ref->{'mode'};
       my $namestr = substr($ref->{'dir'}.$ref->{'name'}, length($curdir));
       $namestr = "<a href='".gen_link("/?r=$repo&c=$cpid&d=$curdir&leech=$ref->{'id'}")."'>$namestr</a>" ;

       print "<tr class='t$ref->{'t'}'><td>$ref->{'ts'}</td><td>$ref->{'t'}</td><td>$mode</td><td>$ref->{'uid'}</td><td>$ref->{'gid'}</td><td>$ref->{'size'}</td><td>$mtime</td><td>$namestr</td></tr>\n";
    }

  print "</tbody></table>";



  } else {


  print "<form method='get' action='/' name='nav'>";
  print "<input type='hidden' name='a' value='$auth_token_to_be_appended'>" if($auth_token_to_be_appended);
  print "<table id='nav'><tr><td>Repository:<br>";

# repo
  print "<select id='r' name='r' onchange='location=\"/?r=\"+this.value'>";
  for my $r (keys %$repos) {
     next if(($conf->{'auth.hmac'})&&($r ne $repo));

     print "<option value='$r' ".($r eq $repo?"selected":"").">$r</option>\n";
  }
  print "</select>";

  print "</td><td>";

# cp
  if($cpid) {
    print "Checkpoint:<br>";
    print "<select id='c' name='c' onchange='document.nav.submit()'>";
    for my $ts (sort keys %$cps_by_ts) {
       my $aid = $cps_by_ts->{$ts};
       my $vts = substr($ts,0,4)."-".substr($ts,4,2)."-".substr($ts,6,2)." ".substr($ts,8,2).":".substr($ts,10,2).":".substr($ts,12,2);
       print "<option value='$aid' ".($aid eq $cpid?"selected":"").">$vts</option>\n";
    }
    print "</select>";
  }

  print "</td><td>Directory:<br>";

# curdir selector
  print "<select id='d' name='d' onchange='document.nav.submit()'>";
  my @parts = split('/', $curdir);
  push @parts, "" if(!scalar @parts);
  my $ps="";
  my $upperdir = "";
  for my $part (@parts) {
     my $aps = "$part/";
     my $pps = $ps;
     $ps .= $aps;

     my $selected = ($ps eq $curdir ? "selected" : "");
     $upperdir = $pps if($selected);

     next if(($conf->{'dirregexp'})&&($ps !~ $conf->{'dirregexp'}));

     print "<option value='$ps' $selected>$ps</option>";
  }
  print "</select>";

  $upperdir = "" if(($conf->{'dirregexp'})&&($upperdir)&&($upperdir !~ $conf->{'dirregexp'}));


#print Dumper(\@parts);print "Y $curdir Y $upperdir Y";

  print "</td><td>";
  if(!$conf->{'dirregexp'}) {
    print "<input type='checkbox' value='1' id='shortpath' name='shortpath'> ";
    print "<label for='shortpath'>Use short pathes instead of full</label><br>";
  }
  print "<input type='submit' value='Download contents below as .tar.gz (recursive)' name='tar'><input type='button' value='Commits' onClick='location=location+\"&commitlog=1\"'></td></tr></table>\n";

  print "</form>";

  my $o = $params->{'o'} || "";
  $o = "name" if($o !~ /^(t|mode|uid|gid|size|mtime|name)$/);
  my $od = $params->{'od'} || 0;

  my $orderby = "ORDER BY $o ".($od?"DESC":"");

  my $iod = !$od;
  print "<table id='files'><thead><tr>
     <th><a href='".gen_link("/?r=$repo&c=$cpid&d=$curdir&o=t&od=$iod")."'>T</a></th>
     <th><a href='".gen_link("/?r=$repo&c=$cpid&d=$curdir&o=mode&od=$iod")."'>Permissions</a></th>
     <th><a href='".gen_link("/?r=$repo&c=$cpid&d=$curdir&o=uid&od=$iod")."'>Owner</a></th>
     <th><a href='".gen_link("/?r=$repo&c=$cpid&d=$curdir&o=gid&od=$iod")."'>Group</a></th>
     <th><a href='".gen_link("/?r=$repo&c=$cpid&d=$curdir&o=size&od=$iod")."'>Filesize</a></th>
     <th><a href='".gen_link("/?r=$repo&c=$cpid&d=$curdir&o=mtime&od=$iod")."'>Mtime</a></th>
     <th><a href='".gen_link("/?r=$repo&c=$cpid&d=$curdir&o=name&od=$iod")."'>Filename</a></th></tr></thead>\n";
  print "<tbody>";

  if($cpid) {
    if($upperdir) {
       print "<tr><td>D</td><td></td><td></td><td></td><td></td><td></td><td><a href='".gen_link("/?r=$repo&c=$cpid&o=$o&od=$od&d=$upperdir")."'>..</a></td></tr>\n";
    }

    my $res = getdiritems($cpid,$latestcpid, $curdir, $orderby);

    while (my $ref = $res->fetchrow_hashref()) {
       next if($ref->{'t'} eq "P");
       next if(($ref->{'t'} eq "L")&&($conf->{'hide_symlinks'}));

       my $mtime = localtime $ref->{'mtime'};
       my $mode =  sprintf "%06o", $ref->{'mode'};
       my $namestr = $ref->{'name'};
       $namestr = "<a href='".gen_link("/?r=$repo&c=$cpid&o=$o&od=$od&d=$curdir$namestr/")."'>$namestr</a>" if($ref->{'t'} eq "D");
       $namestr = "<a href='".gen_link("/?r=$repo&c=$cpid&d=$curdir&leech=$ref->{'id'}")."'>$namestr</a>" if($ref->{'t'} eq "F");
       $namestr .= " -> ".readlink(getfullpath($ref)) if($ref->{'t'} eq "L");
       print "<tr class='t$ref->{'t'}'><td>$ref->{'t'}</td><td>$mode</td><td>$ref->{'uid'}</td><td>$ref->{'gid'}</td><td>$ref->{'size'}</td><td>$mtime</td><td>$namestr</td></tr>\n";
    }
  }


  print "</tbody></table>";

  }

  print <<END;
</body>
</html>
END

}

sub getdiritems {
  my $cpid = shift;
  my $latestcpid = shift;
  my $curdir = shift;
  my $orderby = shift;
  my $recursive = shift || 0;

  my $dirstr = $recursive ? "$curdir%" : $curdir;
  my $dirop = $recursive ? " LIKE " : " = ";

  my @cplist = (0);
  for(my $i = $cpid; $i < $latestcpid; $i++) {
     push @cplist, $i;
  }

     my $res;
     if($cpid ne $latestcpid) {
 
       $res = mqwo("
                   SELECT * FROM files WHERE checkpoint IN (".join(',',@cplist).") AND ocheckpoint <= ? AND dir $dirop ?
          $orderby
                   ", $cpid, $dirstr);
     } else {
       # this is the easiest way
       $res = mqwo("SELECT * FROM files WHERE checkpoint=0 AND dir $dirop ? $orderby", $dirstr);
     }

  return $res;
}

sub getfullpath {
  my $file = shift;
  return "$repodir/".cpdir($file->{'checkpoint'}).$file->{'dir'}.'/'.$file->{'name'};
}

sub initdb {
  return if($dbh);
  my $dbname = validaterepo($repodir);
  die "database not found" if(!$dbname);
  my $dbf = "$repodir/$dbname.db";
  my $dsn = "DBI:SQLite:dbname=$dbf";
  $dbh = DBI->connect($dsn, "", "") or die("cant open db");
  $dbh->{AutoCommit}=0;
  $dbh->{RaiseError}=1;
  $dbh->{sqlite_see_if_its_a_number}=1;

}

my %cpdircache;
sub cpdir {
  my $cp = shift;

  return "latest" if(!$cp);
  return $cpdircache{$cp} if($cpdircache{$cp});
  my $fn =getcpfilename(gc("SELECT ts FROM checkpoint WHERE id=?",$cp));
  $cpdircache{$cp} = $fn;
  return $fn;
}

sub lastcps {
  my $limit = shift;
  my $limitstr = "";
  $limitstr = " LIMIT $limit " if($limit);

  my %re1;
  my %re2;
  my $latestcpid;
  my $sth = mqwo("SELECT id, ts FROM checkpoint WHERE ts != 'latest' ORDER BY ts DESC $limitstr");
  while(my $ref = $sth->fetchrow_hashref()) {
    $latestcpid = $ref->{'id'} if(!$latestcpid);
    my $ts = getcpfilename($ref->{'ts'});
    $re1{$ts} = $ref->{'id'};
    $re2{$ref->{'id'}} = $ts;
  }
  $sth->finish;
  return (\%re1,\%re2, $latestcpid);
}


sub firstkey {
  my $r = shift;
  my @keys = keys %$r;
  return if(scalar @keys < 1);

  return $keys[0];
}

sub validaterepos {
  my %re;
  for my $c (@{$conf->{'repos'}}) {
     if($conf->{'subrepo_mode'}) {
        opendir(my $dh, $c);
        if(!$dh) {
           warn "cant open repo: $c";
           next;
        }
        while(my $an = readdir($dh)) {
           next if($an !~ /^[\w\d\-_]+$/);
           if(my $n = validaterepo("$c/$an")) {
              $re{$an} = "$c/$an";
           }
        }
        closedir($dh);
     }
     elsif(my $n = validaterepo($c)) {
        $re{$n} = $c;
     }
  }
  return \%re;
}
sub validaterepo {
  my $c = shift;
  opendir(my $X, $c);
  my @r = grep {/(.+)\.db$/} readdir($X);
  closedir($X);

  
  return if(scalar @r != 1);
  my $re = $r[0];
  $re =~ s/\.db$//g;
  return $re;
}

sub authrequired {
  my $realm = shift || "Realm";
  print "Status: 401 Authentication required\r\n";
  print "WWW-Authenticate: Basic realm=\"$realm\"\r\n\r\n";
}

sub gen_link {
  my $location = shift;
  $location .= "&a=$auth_token_to_be_appended" if($auth_token_to_be_appended);
  return $location;
}

sub redirect {
  my $location = gen_link(shift);
  print "Status: 302 Redirected\r\n";
  print "Connection: close\r\n";
  print "Location: $location\r\n\r\n";
  print "Redirected to: $location";
}

sub parse_query {
  my $buffer = shift|| return {};

  my %in;
    my  @pairs = split(/&/, $buffer);
      foreach my $pair (@pairs){
           my  ($name, $value) = split(/=/, $pair);
           $in{$name} = urldecode( $value ) ;
      }

  return \%in;

}

sub urldecode {
    my $s = shift||"";
    $s =~ s/\%([A-Fa-f0-9]{2})/pack('C', hex($1))/seg;
    $s =~ s/\+/ /g;
    return $s;
}
sub urlencode {
    my $s = shift || "";
    $s =~ s/ /+/g;
    $s =~ s/([^A-Za-z0-9\+-])/sprintf("%%%02X", ord($1))/seg;
    return $s;
}

sub getcredentials {
  my($un,$pw);
  my $a = $ENV{"HTTP_AUTHORIZATION"};
  if(($a)&&($a=~ /Basic (.+)/i)) {
     my $x = decode_base64($1);
     if($x =~ /(.+):(.+)/) {
        $un = $1;
        $pw = $2;
     }
  }
  return($un,$pw);
}

sub trim {
  my $v = shift;
  $v =~ s/\s*$//g;
  return $v;
}