#!/usr/bin/perl

use DBI;
use warnings;
use strict;
use Net::FTP;
use DBD::SQLite;
use Log::Log4perl qw(:easy);
use Data::Dumper;
use FindBin qw($Bin);
use IO::Select;
use IO::Socket::SSL;
use IO::Socket::INET;
use FileHandle;
use Fcntl ':mode';
use Errno ':POSIX';
use Time::HiRes qw( time );
use vars qw($dbh);
BEGIN {
 push @INC, "$Bin";
}
use Archive::SzarStream;
use DaemonizeChroot;

##################################################################################
require "$Bin/szar-backup-common.pl";

my $SZARBACKUP_VERSION = "1.0.1";

my %commands = (
  "help"=> {
     help => "Displays help about a command.\nParams: commandname",
     execute => \&cmd_help
  },
  "rcheckpoints" => {
     help => "Lists backups checkpoints on the remote side.",
     execute => \&cmd_rcheckpoints
  },
  "rcompare" => {
     help => "Show a list which remote files would be restored with the restore command.\nUsage: rcompare vdir [time_expr]",
     execute => \&cmd_rcompare
  },
  "test" => {
     help => "Tests the connection to the remote side",
     execute => \&cmd_test
  },
  "backup" => {
     help => "Performs (incremental) backup",
     execute => \&cmd_backup
  },
  "fullbackup" => {
     help => "Performs full backup",
     execute => \&cmd_fullbackup
  },
  "cleanup" => {
     help => "Deletes 'old' files of old checkpoints as defined at server side.",
     execute => \&cmd_cleanup
  },
  "del-stalled" => {
     help => "Deletes stalled repositories as defined at the server side.",
     execute => \&cmd_delstalled
  },
  "compare" => {
     help => "Compare files and show the list which files would need to backup.",
     execute => \&cmd_compare
  },
  "delete" => {
     help => "Deletes checkpoints specified by the expression.\nDeletes older ones as the specified as well. \nUsage: delete time_expression|checkpointtimestamp",
     execute => \&cmd_delete
  },
  "restore" => {
     help => "Restores remote backup\nUsage: restore vdir destpath [time_expression]",
     execute => \&cmd_restore
  },
);

my $firsttime = 1;
my ($select, $sock, $connected);
my ($sent_size,$read_size,$sessions, $lastio, $proto_errors) = (0, 0, 0, 0, 0);
my ($xfer_read_bytes, $xfer_read_secs) = (0, 0);
my ($xfer_sent_bytes, $xfer_sent_secs) = (0, 0);
my $conffile = $ARGV[0];
my $cmd = $ARGV[1];
if((!$cmd)||(!$commands{$cmd})) {
  Usage();
  exit;
}
shift @ARGV;
shift @ARGV;

die "Cant find conf file" if((!$conffile)||(!-s $conffile));

$| = 1;
my $shouldbreak = 0;
$SIG{__WARN__} = sub { WARN(@_); };
$SIG{__DIE__} = sub { FATAL(@_); };
$SIG{PIPE} = "IGNORE";
die("At least DBD::SQLite 1.32_02 is needed, you have $DBD::SQLite::VERSION") if($DBD::SQLite::VERSION lt "1.32_02");

INFO("szar-Backup v$SZARBACKUP_VERSION");
my $conf = parseconf();
Log::Log4perl::init( \$conf->{'log4perl'} ) or die("Cant parse log conf");
$commands{$cmd}{execute}();

sub initdb{
  return if($dbh);
  my $dsn = "DBI:SQLite:dbname=".getdbpath();
  $dbh = DBI->connect($dsn, "", "") or die("cant open db");
  $dbh->{AutoCommit}=0;
  $dbh->{RaiseError}=1;
  $dbh->{sqlite_see_if_its_a_number}=1;
  for my $sql (@{$conf->{'dbinit'}}) {
    $dbh->do($sql);
  }

}
sub cmd_delete {
  my $ts = join(' ',@ARGV);
  die("Timestamp argument is missing")  if(!$ts);

  dbfetch();
  my %todel;
  my $tsd = parsets($ts);
  my $sth = mqwo("SELECT * FROM checkpoint WHERE ts <= ?", $tsd);
  while(my $ref = $sth->fetchrow_hashref()) {
    $todel{$ref->{'id'}} = $ref->{'ts'};
  }
  $sth->finish();

  if(!scalar keys %todel) {
    INFO "Nothing to delete";
    return;
  }


  INFO "Deleting:";
  for my $id (sort keys %todel) {
    my $s = "\t$id ".$todel{$id};
    INFO $s;
  }
  INFO "If you are sure, enter your password:";
  my $pw = <STDIN>;
  $pw = trim($pw);
  return  if(!$pw);

  mywrite("AUTH $pw");
  my $a = myread();
  die("Incorrect password") if ($a !~ /^OK/);

  for my $id (sort keys %todel) {
     my $ts = getcheckpointts($id);
     mywrite("DELCP $ts");
     $a = myread();
  }
  unlink(getdbpath()); # pf.
}
sub getdbname {
  opendir(my $d, $conf->{'dbdir'}) or die ("couldnt open dbdir");
  my $filter = ".db";
  $filter = "-$conf->{'subrepo'}$filter" if($conf->{'subrepo'});
  my @dbs = grep{/\Q$filter\E$/} readdir($d);
  closedir($d);
  die("more than one db found!") if(scalar @dbs > 1);
  return "" if(!scalar @dbs );
  return $dbs[0];

}
sub getdbpath {
  my $d = getdbname();
  if(!$d) {
     $d = $conf->{'name'};
     $d .= "-".$conf->{"subrepo"} if($conf->{"subrepo"});
     $d .= ".db";
  }
  return $conf->{'dbdir'}.'/'.$d;
}

sub myconnect {
  return if($connected);
  INFO "Connecting to $conf->{'peeraddr'}:$conf->{'peerport'}";
  if($conf->{'ssl'}) {
   $sock = IO::Socket::SSL->new( PeerAddr => $conf->{'peeraddr'},
                                   PeerPort => $conf->{'peerport'},
                                   Proto    => 'tcp',
                                   SSL_use_cert => 1,
                                   SSL_verifycn_scheme => $conf->{'ssl.verifycn_scheme'},
                                   SSL_ca_file => $conf->{'ssl.ca_file'},
                                   SSL_key_file => $conf->{'ssl.key'},
                                   SSL_cert_file => $conf->{'ssl.crt'},
                                   SSL_verify_mode => 0x01, # verify peer
                                   SSL_passwd_cb => sub { return $conf->{'ssl.passphrase'} },
                                 )
    or
    die("unable to create socket: ". (&IO::Socket::SSL::errstr) ." $@") ;

  # check server cert.
  my $subject_name = $sock->peer_certificate("subject");
  my $issuer_name = $sock->peer_certificate("issuer");
  my $cipher = $sock->get_cipher();

    if($firsttime) {
      $firsttime = 0;
      INFO "SERVER cipher: $cipher";
      INFO "SERVER subject: $subject_name";
      INFO "SERVER issuer $issuer_name";
    }
  } else {
   $sock = IO::Socket::INET->new( PeerAddr => $conf->{'peeraddr'},
                                   PeerPort => $conf->{'peerport'},
                                   Proto    => 'tcp',
                                 )
    or
    die("unable to create socket: $@") ;

    DEBUG "Connection established";
  }


  $connected = 1;


  binmode $sock;
  $sock->autoflush();

  $select = IO::Select->new();
  $select->add($sock->fileno());

  my $banner = trim(myread());
  die "Server is running different version of szar-Backup than me: $banner"  if($banner !~ /$SZARBACKUP_VERSION$/);

  if($conf->{"subrepo"}) {
    mywrite("SUBREPO ".$conf->{"subrepo"});
    my $a = trim(myread());
    die "subrepo was not accepted by the server" if($a !~ /^OK/);
  }

  return( $banner );

}
sub mywrite {
  my $msg = shift;

  myconnect();

  my @ready =$select->can_read(0);
  die "server is disconnected or protocol is out of sync" if(scalar @ready);

  @ready =$select->can_write($conf->{'timeout'});

  if(!scalar @ready) {
    die("Server is not writeable within timeout");
  }
  print $sock "$msg\r\n" or  die "Write error: $! $? $SSL_ERROR" ;
  DEBUG ">> $msg";
  $sent_size += length($msg)+2;
  $lastio = time();
}
sub myread {
  myconnect();

  my $f = "";
  while(1) {
    my @ready = $select->can_read($conf->{'timeout'});
    if(!scalar @ready) {
       die "Server is not readable within timeout";
    }

      my $abuf = "";
      my $n = read( $sock,$abuf,$conf->{'read_buffer_size'});
      if ( !defined($n)) {
        die( "Read error: $? $! $SSL_ERROR" );
      } elsif ( $n == 0 ) {
        die( "Connection closed" );
      }
      DEBUG "<< ".trim($abuf);
      $read_size += $n;
      $f .= $abuf;
      $lastio = time();
      if($f =~ /^(OK|NO)/m) {
           if($f =~ /^NO/m) {
                $proto_errors++;
                ERROR $f
          }
          return $f;
      }
  }
}
sub keepalive {
  return if(!$connected);
  return if(!$lastio);
  my $now = time();
  if($now - $lastio > $conf->{'maxidle'}) {
     mywrite("TEST");
     myread();
  }
}
sub cmd_test {
  parse_noparams();

  mywrite("TEST");
  my $a = trim(myread());
  if($a=~ /^OK/) {
    INFO "Everything's fine." ;
  }
}
sub mygetb {
  my ($destdir, $method, $expectedfiles, $stripdirs, $forcefilename) = @_;

  my $xfer_began = time();
  my $extracted = 0;
  eval {
    my $szar = Archive::SzarStream->read_handle($sock, {
        timeout=>$conf->{'timeout'},
        dir=>$destdir,
        debug=>$conf->{'szar_debug'},
        compression=>$method,
        overwrite=>$conf->{'overwrite'},
        do_chmod_files=>1,
        do_chmod_dirs=>1,
        do_chown=>$stripdirs && !$<,
        do_utime=>1,
    });
    while(my $e = $szar->read_entry()) {
        my $eto = "";
        $eto = "$destdir/$1"  if(($stripdirs)&&($e->{'filename'} =~ /^(?:latest|\d{14})$stripdirs(.+)/));
        $eto = $forcefilename if($forcefilename);
        DEBUG "Extracting $e->{'filename'} $eto";
        if(!$szar->extract($eto)) {
            ERROR "Error extracting $eto";
        }

        $extracted++;
    }
    DEBUG "Extraction ready";

    $read_size += $szar->{'read_bytes'};
    $xfer_read_bytes += $szar->{'read_bytes'};

  };
  die($@) if($@);

  $xfer_read_secs += time()-$xfer_began;

  my $a = myread();
  die("Protocol error after GET") if($a !~ /OK (\d+)/);
  my $serversent = $1;

  if($serversent != $expectedfiles) {
     $proto_errors++;
     ERROR "Server sent $serversent files, we expected $expectedfiles";
  }

  if($serversent != $extracted) {
     $proto_errors++;
     ERROR "Server sent $serversent files, we extracted $extracted";
  }

  return ($serversent, $extracted);

}
sub myget {
  my ($destdir, $files, $method, $stripdirs) = @_;

  return if(!scalar @$files);

  $sessions++;

  $method = $conf->{'get'} if(!$method);


  # aztan letoltjuk.
  mywrite("GET $method ".(scalar @$files));
  my $a = trim(myread());
  die "server didnt accept the GET request" if($a !~ /^OK/);

  for my $f (@$files) {
    mywrite($f->{'id'});
  }
  mywrite( "READY");

  $a = trim(myread());
  die "server didnt complete the GET request" if($a !~ /^OK (\d+)/);
  my $sc =$1;
  if($sc != scalar @$files) {
     ERROR "Server got $sc files only for GET, we sent ".(scalar @$files);
     $proto_errors++;
  }

  return mygetb($destdir, $method, scalar @$files, $stripdirs);
}

sub disconnect {
  if($connected) {
    $connected = 0;
    $lastio = 0;
    close($sock);
  }
}
sub dbfetch_realwork {
  mywrite("CPLATEST $conf->{'name'}");
  my $a = myread();
  if($a !~ /^OK (.+)\s*/) {
    die("protocol error in dbfetch");
  }
  my $cpts = $1;
  $cpts = trim($cpts);

  my $wdb = "$cpts";
  $wdb .= "-$conf->{'subrepo'}" if($conf->{'subrepo'});
  $wdb .= ".db";
  my $adbname = getdbname();
  if($wdb eq $adbname) {
     DEBUG "We have the same database version already";
     return;
  }

  my $dbpath = getdbpath();
  if(-s $dbpath) {
     unlink($dbpath);
  }

  INFO "Fetching latest DB";

  mywrite("DB ".$conf->{'db_fetch_method'});
  $a = myread();
  if($a !~ /^OK/) {
    die("Couldnt fetch the db");
  }

  my $tmp = $conf->{'dbdir'}."/tmp-$conf->{'name'}";
  $tmp .= "-$conf->{'subrepo'}" if($conf->{'subrepo'});
  $tmp .= ".db";

  my $x = mygetb($conf->{'dbdir'}, $conf->{'db_fetch_method'}, 1, undef, $tmp);
  die "Failed to fetch database" if(!$x);
  my $dst = $conf->{'dbdir'}."/$wdb";
  rename($tmp, $dst) if($tmp ne $dst);
  INFO "DB fetched successfully.";
}

sub dbfetch {
  dbfetch_realwork();

  if($conf->{"chroot"}) {
     DEBUG "Opening database before chrooting";
     gc("SELECT DATE('now')");

     DEBUG "Chrooting to $conf->{'chroot'}";
     chroot($conf->{"chroot"}) or die "Couldnt chroot to $conf->{'chroot'}: $!";
     chdir("/");
  }

  if ($conf->{'user'}) {
     DEBUG "Switching user to $conf->{'user'}:$conf->{'group'}";
     DaemonizeChroot::set_user($conf->{'user'}, $conf->{'group'}) ;
  }

}

sub parseconf {
  my %conf;
  my @required = ("name","peeraddr","peerport","dbdir","backupdir","ssl.key","ssl.crt","ssl.ca_file");
  my $tar =trim(`which tar`);
  my %optional = (
     "db_fetch_method"=>"GZ",
     "szar_debug"=>0,
     "compression_level"=>9,
     "put"=>"GZ",
     "get"=>"GZ",
     "maxidle"=> 10,
     "put_filesize_fallback"=>0,
     "ssl.passphrase"=>"",
     "ssl.verifycn_scheme"=>"http",
     "move_commit" => 500,
     "upload_commit" => 3000,
     "restore_commit" => 3000,
     "showstats"=> 1,
     "timeout"=>30,
     "overwrite"=>0,
     "ssl" => 1,
     "read_buffer_size"=>32768,
     "subrepo"=> "",
     "chroot"=> "",
     "user"=>"",
     "group"=>"",
  );
  my @stuffs = (@required, keys %optional);

  my $crestr = "^(".join('|',@stuffs).")\\s*=\\s*(.+)";

  my @dbinitsqls;
  my @conflines;
  open(my $x, "<$conffile") or die("Cant open conf file");
  while(<$x>) {
     push @conflines, $_;
  }
  close($x);
  for my $k (keys %ENV) {
     next if($k !~ /^SZAR_(.+)/);
     my $v =$ENV{$k};
     push @conflines, "$1=$v\n";
  }
  for (@conflines) {
    next if(/^\s*$/);
    next if(/^#/);
    if(/^log4perl/) {
       $conf{'log4perl'} .= $_;
       next;
    }

    # kulonben harapunk
    if(/$crestr/o) {
      $conf{$1} = $2
    }
    elsif(/^exclude(.*)\s*=\s*(.+)/) {
      my $subdir = $1;
      my $xs = $2;
      $subdir = trim($subdir);
      $subdir .= '/' if(($subdir)&&($subdir !~ /\/$/));
      push @{$conf{"exclude$subdir"}}, qr/$xs/;
    }
    elsif(/^include(.*)\s*=\s*(.+)/) {
      my $subdir = $1;
      my $xs = $2;
      $subdir = trim($subdir);
      $subdir .= '/' if(($subdir)&&($subdir !~ /\/$/));
      push @{$conf{"include$subdir"}}, qr/$xs/;
    }
    elsif(/^put_exts_(SZAR|GZ|BZ2)\s*=\s*(.+)/) {
       my $method = $1;
       my $v = $2;
       $conf{"put_exts_$method"} = parseexts($v);
    }
    elsif(/^dbinit\s*=\s*(.+)/) {
       push @dbinitsqls, $1;
    }
    else {
      die("Invalid line in the config file: $_");
    }
  }
  close($x);

  $conf{"dbinit"} = \@dbinitsqls;
  for my $k (@required) {
    die("No value for $k in conf file!") if(!defined($conf{$k}));
  }
  die "Unknown PUT method" if($conf{'put'} !~ /^(SZAR|GZ|BZ2)$/);
  die "Unknown PUT method" if($conf{'get'} !~ /^(SZAR|GZ|BZ2)$/);

  for my $k (keys %optional) {
    $conf{$k} = $optional{$k} if(!defined($conf{$k}));
  }

  $conf{'backupdir'} =~ s/\/*$//g;
  $conf{'backupdir'} = "/" if(!$conf{'backupdir'});

  die("cant read backupdir: $conf{'backupdir'}") if(!-d $conf{'backupdir'});
  die("cant write dbdir dir: $conf{'dbdir'}") if((!-d $conf{'dbdir'})||(!-w $conf{'dbdir'}));

#  print Dumper(\%conf);exit;

  return \%conf;
}

sub parseexts {
  my $exts = shift;
  my %re;
  for my $ext (split /\|/, $exts) {
    $re{lc($ext)}= 1;
  }
  return \%re;
}


sub Usage {
  print "Usage: $0 conf_file command [args]\n";
  print "Valid commands: ".join(", ",sort keys %commands)."\n";
}


sub parse_noparams {
  die "This command takes no arguments." if(scalar @ARGV);
}


sub cmd_help {
  if(scalar @ARGV != 1) {
    die "Usage: $0 conf_file help command";
  }
  if(!$commands{$ARGV[0]}) {
    ERROR "Unknown command: ".$ARGV[0];
    exit;
  }
  INFO $commands{$ARGV[0]}->{help};
}

sub comparefiles {
  my ($re, $realbasedir)  = @_;

  my @dirstoopen= ('/');
  while(1) 
  {
	 keepalive() if(!$re->{'compareonly'});

	my $virtualpath = shift @dirstoopen;
	return if(!$virtualpath);


	my $realpath = $realbasedir.$virtualpath;

      my $v = opendir(my $X, $realpath);
      if(!$v) 
      {
	 $re->{'warnings'}++;
	 WARN "Cant opendir $realpath $!";
	 next;
      }
      
     my %ondisk;
      while(my $f = readdir($X)) 
      {
	 keepalive() if(!$re->{'compareonly'});

	 next if($f =~ /^\.\.?$/);

	 my $fullvirtualpath = "$virtualpath$f";
	 my $fullrealpath = "$realpath$f";

	 my $shouldinclude = 1;
	 if(defined($conf->{"exclude$virtualpath"})) 
	 {
	       for my $exclude (@{$conf->{"exclude$virtualpath"}}) 
	       {
		     if($fullvirtualpath =~ /$exclude/) 
		     {
		       DEBUG "Skipping $fullvirtualpath cause of pattern $exclude";
		       $shouldinclude = 0;
		       last;
		     }
	       }
	 }
	 if($shouldinclude) 
	 {
	       for my $exclude (@{$conf->{'exclude'}}) {
		    if($fullvirtualpath =~ /$exclude/) {
		       DEBUG "Skipping $fullvirtualpath cause of pattern $exclude";
		       $shouldinclude = 0;
		       last;
		    }
	       }
	 }
	 if(!$shouldinclude) 
	 {
	       if(defined($conf->{"include$virtualpath"})) {
		 for my $include (@{$conf->{"include$virtualpath"}}) {
		  if($fullvirtualpath =~ /$include/) {
		     DEBUG "Including $fullvirtualpath cause of pattern $include";
		     $shouldinclude = 1;
		     last;
		   }
		 }
	       }
	       if(!$shouldinclude) 
	       {
		 for my $include (@{$conf->{'include'}}) {
		  if($fullvirtualpath =~ /$include/) {
		     DEBUG "Including $fullvirtualpath cause of pattern $include";
		     $shouldinclude = 1;
		     last;
		   }
		 }
	       }
	 }
	if (!$shouldinclude) {
	     $re->{'excludeskips'}++;
	     next ;
	}

	# most megnezzuk adatbazisban van e ilyen
	my @sts = lstat($fullrealpath);
	if(!scalar @sts) { $re->{'warnings'}++; WARN "Cant stat file $fullvirtualpath: $!"; next; }


	my %st;
	$st{'fullvirtualpath'} = $fullvirtualpath;
	$st{'size'} = $sts[7];
	$st{'atime'} = $sts[8];
	$st{'mtime'} = $sts[9];
	$st{'mode'} = $sts[2];
	$st{'uid'} = $sts[4];
	$st{'gid'} = $sts[5];
	$st{'rdev'} = $sts[6];

	$st{'t'} = "O";
	$st{'t'} = "F" if(S_ISREG($sts[2]));
	$st{'t'} = "D" if(S_ISDIR($sts[2]));
	$st{'t'} = "L" if(S_ISLNK($sts[2]));

	$st{'dir'} = $virtualpath;
	$st{'name'} = $f;

	    if($st{'t'} eq "D")  {
               push @dirstoopen, "$fullvirtualpath/" ;
            }

	
	$ondisk{$f} = \%st;		
      }
      closedir($X);

      my %indb;
      
      # megmegnezzk hogy voltake torlesek
      if(!$re->{'fullbackup'})
      {
	my $sd = mqwo("SELECT * FROM files WHERE checkpoint=0 AND dir=?", $virtualpath);
	while (my $ref = $sd->fetchrow_hashref()) {
	   keepalive() if(!$re->{'compareonly'});

	   $indb{$ref->{'name'}} = $ref;
	   
	  if(!$ondisk{$ref->{'name'}}) {
	     my $dline = "DEL $ref->{'t'}: $virtualpath$ref->{'name'}";
	     if(!$re->{'compareonly'}) {
	       DEBUG $dline;
	       myaddmove($re, $ref);
	     } else {
	       INFO $dline;
	     }
	     $re->{'deletedfiles'}++;
	     $re->{'deletedfilesize'}+=$ref->{'size'};
	  }
	}
	$sd->finish();
      }
      
      # es most megnezzuk volt e valtozas vagy uj feltoltest
      for my $f (keys %ondisk) 
      {
      
            my $st = $ondisk{$f};
	    my $fullvirtualpath = $st->{'fullvirtualpath'};
		    
	    my $ref = $indb{$f};
		    
	   if(($ref)&&($ref->{'id'})) 
	   {
	      $st->{'id'} = $ref->{'id'};

	      if($ref->{'t'} ne $st->{t})
	      {
		 DEBUG "Changed file type $fullvirtualpath: $ref->{'t'} -> $st->{'t'}";
	      }
	      elsif($ref->{'t'} eq "D") 
	      {
		 # directories needs to be treated differently
		 # at first, we dont care about size at all
		 # and other parameter changes will be treated as a phantom entry in the db.
		 if(($ref->{'uid'}!=$st->{'uid'})||($ref->{'gid'}!=$st->{'gid'})||($ref->{'mtime'}!=$st->{'mtime'})) {
		    if($re->{'compareonly'}){
		      INFO("PHT $ref->{'t'}: $fullvirtualpath"); # phantomdir
		    } else{
		      DEBUG("PHT $ref->{'t'}: $fullvirtualpath");
		    }
		    if(!$re->{'compareonly'}) {
		      myaddphantoms($re, $st);
		    }
		 } else {
		    # completly the same, dont need to touch the db
		    DEBUG("UNC $ref->{'t'}: $fullvirtualpath");
		    $re->{'unchangedfiles'}++;
		    # $re->{'unchangedfilesize'}+= 0;
		 }
		 next;
	      }
	      elsif(
		    ($ref->{'rdev'} == $st->{'rdev'})
		    &&
		    ($ref->{'uid'} == $st->{'uid'})
		    &&
		    ($ref->{'mode'} == $st->{'mode'})
		    &&
		    ($ref->{'gid'} == $st->{'gid'})
		    &&
		    ($ref->{'size'} == $st->{'size'})
		    &&
		    ($ref->{'mtime'} == $st->{'mtime'})
		   ) {
		DEBUG("UNC $ref->{'t'}: $fullvirtualpath");
		$re->{'unchangedfiles'}++;
		$re->{'unchangedfilesize'}+= $st->{'size'};
		next;
	      }
	   }




	    my $fokat = "";
	    if($st->{'id'}) {
	      $fokat = "CHG";
	      $st->{'id'} = $ref->{'id'};
	      # ezen a ponton ez direktory nem lehet (azt az esetet feljebb mar lekezeltuk)
	      myaddmove($re, $st);

	      $re->{'changedfiles'}++;
	      $re->{'changedfilesize'}+= $st->{'size'};
	    } else {
	      $fokat = "NEW";

	      $re->{'newfiles'}++;
	      $re->{'newfilesize'}+= $st->{'size'};
	    }

	    myadduploads($re, $st, $fokat, $fullvirtualpath);		
      
      }
  }
}


sub showduration {
  my $s = shift;
  my $h = int( $s / 3600 );
  $s -= $h *3600;
  my $m = int ( $s / 60 );
  $s -= $m * 60;
  my $ts = "";
  $ts .= "$h hours " if($h);
  $ts .= "$m minutes " if($m);
  $ts .= sprintf("%d",$s)." seconds " if(($s)||(!$ts));
  return trim($ts);
}
sub showsize {
  my $b = shift;
  $b = sprintf("%d", $b);
  my $bs = "$b bytes";
  if($b > 1024*1024*1024) {
    $bs = sprintf("%.1f gbytes ($bs)", ($b / 1024 / 1024 / 1024));
  }
  elsif($b > 1024*1024) {
    $bs = sprintf("%.1f mbytes ($bs)", ($b / 1024 / 1024));
  }
  elsif($b > 1024) {
    $bs = sprintf("%.1f kbytes ($bs)", ($b / 1024));
  }

  return $bs;
}
sub showspeed {
  my $b = shift;
  $b = sprintf("%d", $b);
  my $bs = "$b bytes";
  if($b > 1024*1024*1024) {
    $bs = sprintf("%.1f gbytes", ($b / 1024 / 1024 / 1024));
  }
  elsif($b > 1024*1024) {
    $bs = sprintf("%.1f mbytes", ($b / 1024 / 1024));
  }
  elsif($b > 1024) {
    $bs = sprintf("%.1f kbytes", ($b / 1024));
  }
  $bs .= "/sec";

  return $bs;
}
sub showcomparestats {
  my $c = shift;
  return if(!$conf->{'showstats'});

  return if(!$c->{'comparebegin'});

  if($c->{'compareonly'}){
    INFO "------------------------------------ Compare";
  } else {
    INFO "------------------------------------ Session";
  }
  my $secs = $c->{'compareend'}-$c->{'comparebegin'};
  INFO "Elapsed time: ".showduration($secs) if($c->{'compareend'});
  INFO "Compare warnings: ".$c->{'warnings'};

  INFO "Excluded files: ".$c->{'excludeskips'};
  INFO "Unchanged files: ".$c->{'unchangedfiles'}. ", ".showsize($c->{'unchangedfilesize'});
  INFO "Changed files: ".$c->{'changedfiles'}. ", ".showsize($c->{'changedfilesize'});
  INFO "New files: ".$c->{'newfiles'}. ", ".showsize($c->{'newfilesize'});
  INFO "Deleted files: ".$c->{'deletedfiles'}. ", ".showsize($c->{'deletedfilesize'});
  INFO "Increment files (new+changed): ".($c->{'newfiles'}+$c->{'changedfiles'}). ", ".showsize($c->{'newfilesize'}+$c->{'changedfilesize'});
  INFO "Source files (new+changed+unchanged): ".($c->{'newfiles'}+$c->{'changedfiles'}+$c->{'unchangedfiles'}). ", ".showsize($c->{'newfilesize'}+$c->{'changedfilesize'}+$c->{'unchangedfilesize'});


  return if($c->{'compareonly'});

#  INFO "DB size: ".showsize(getdbsize());
  INFO "Transfer warnings: ".$c->{'transferwarnings'};
  INFO "Protocol errors: ".$proto_errors;
  INFO "Sessions: ".$sessions; # no multiple single sessions anymore
  if($sessions) {
    INFO "Avg sent bytes / session: ".showsize($sent_size/$sessions);
    INFO "Avg read bytes / session: ".showsize($read_size/$sessions);
  }
  showxferstats();
}
sub getdbsize {
  return -s getdbpath();
}
sub myaddmove {
  my ($re, $ref) = @_;
  return if($re->{'compareonly'});
#  return if(!$ref->{'m'});
  push @{$re->{'tomoves'}}, $ref;
  mymoves($re) if(scalar @{$re->{'tomoves'}} >= $conf->{'move_commit'});
}
sub mymoves {
  my $re = shift;
  return if($re->{'compareonly'});
  return if(!scalar @{$re->{'tomoves'}});

  mywrite("MOVE");
  my $x = myread();
  die("Server doesnt accept our move request") if($x !~ /^OK/);
  for my $ref (@{$re->{'tomoves'}}) {
    mywrite("$ref->{'id'}");
  }
  mywrite("READY");

  $a = myread();
  die "Protocol error in MOVE"  if($a !~ /^OK (\d+)/m);
  my $sc = $1;
  if($sc != scalar @{$re->{'tomoves'}}) {
      ERROR "Server did $sc moves, we wanted ".(scalar @{$re->{'tomoves'}});
      $proto_errors++;
  }

  @{$re->{'tomoves'}} = ();
}
sub myaddphantoms {
  my ($re, $ref) = @_;
  return if($re->{'compareonly'});
  push @{$re->{'tophantoms'}}, $ref;
  myphantoms($re) if(scalar @{$re->{'tophantoms'}} >= $conf->{'move_commit'});
}
sub myphantoms {
  my $re = shift;
  return if($re->{'compareonly'});
  return if(!scalar @{$re->{'tophantoms'}});
  mywrite("PHANTOM");
  my $a = myread();
  die("no phantom?") if($a !~ /^OK/);

  for my $ref (@{$re->{'tophantoms'}}) {
    mywrite($ref->{'id'}." ".$ref->{'mtime'}." ".$ref->{'atime'}." ".$ref->{'mode'}." ". $ref->{'uid'}." ". $ref->{'gid'}." ". $ref->{'size'});
  }
  mywrite("READY");
  $a = myread();
  die "Protocol error in phantom"  if($a !~ /^OK (\d+)/);
  my $sc = $1;
  if($sc != scalar @{$re->{'tophantoms'}}) {
    ERROR "Server created $sc phantoms, we sent ".(scalar @{$re->{'tophantoms'}});
    $proto_errors++;
  }
  @{$re->{'tophantoms'}} = ();
}


sub compare {
  my $kind = shift;

  my %re;

  die("Cant open backupdir: $conf->{'backupdir'}!") if(!-d $conf->{'backupdir'});
  $re{'compareonly'} = 1 if($kind eq "compare");
  $re{'incbackup'} = 1 if($kind eq "inc");
  $re{'fullbackup'} = 1 if($kind eq "full");
  $re{'backup'} = 1 if(!$re{'compareonly'});
  $re{'transferwarnings'} = 0;
  $re{'warnings'} = 0;
  $re{'excludeskips'} = 0;
  $re{'newfiles'} = 0;
  $re{'newfilesize'} = 0;
  $re{'changedfiles'} = 0;
  $re{'changedfilesize'} = 0;
  $re{'unchangedfiles'} = 0;
  $re{'unchangedfilesize'} = 0;
  $re{'deletedfiles'} = 0;
  $re{'deletedfilesize'} = 0;
  @{$re{'tomoves'}} = ();
  @{$re{'tophantoms'}} = ();
  @{$re{'touploads_SZAR'}} = ();
  @{$re{'touploads_GZ'}} = ();
  @{$re{'touploads_BZ2'}} = ();

  dbfetch();

  $re{'comparebegin'} = time();  # cheating

  $re{'cp'} = gcaa("SELECT * FROM checkpoint WHERE id != 0 ORDER BY ts DESC LIMIT 1");
  if($re{'cp'}->{'id'}) {
     INFO "Comparing to checkpoint $re{'cp'}->{'id'} ".$re{'cp'}->{'ts'}."\n";
  } else {
     INFO "Nothing to compare to... doing it in the full way\n";
     $re{'fullbackup'} = 1; # no dummy db queries
  }


  # eloszor be kell szurni egy uj checkpointot
  if(!$re{'compareonly'}) {
#exit;
    my $cmd = "CPNEXT";
    $cmd = "CPNEW" if($re{'fullbackup'});
    mywrite( $cmd );
    my $a = myread();
    die("Couldnt create new checkpoint at server side") if($a !~ /^OK/);

  }


  comparefiles(\%re, $conf->{'backupdir'});
  mymoves(\%re) if(scalar @{$re{'tomoves'}});
  myphantoms(\%re) if(scalar @{$re{'tophantoms'}});
  myuploads("SZAR", \%re) if(scalar @{$re{'touploads_SZAR'}}); # befejezzuk a feltolteseket
  myuploads("GZ", \%re) if(scalar @{$re{'touploads_GZ'}}); # befejezzuk a feltolteseket
  myuploads("BZ2", \%re) if(scalar @{$re{'touploads_BZ2'}}); # befejezzuk a feltolteseket
  $re{'compareend'} = time();

  return \%re;
}
sub displayentry {
  my $e = shift;
       my $f = "$e->{'dir'}$e->{name}";
       INFO $f;
}
sub cmd_compare {
  parse_noparams();

  # felepitjuk
  my $compared = compare("compare");
#print Dumper($compared);

  showcomparestats($compared);
}

my %cptscache;
sub getcheckpointts {
  my $cpid = shift;
  if(!$cptscache{$cpid}) {
    $cptscache{$cpid} = gc("SELECT ts FROM checkpoint WHERE id=?",$cpid) ;
    $cptscache{$cpid} =~ s/(\-| |:|\.)//g;
  }
  return $cptscache{$cpid};
}
my $putnumber = 0;
sub myput {
  my ( $u, $method) = @_;

  return  if(!scalar @$u);

  $sessions++;
  $putnumber++;
  $method = $conf->{'put'} if(!$method);

  INFO "PUT#$putnumber, ".(scalar @$u)." files, method $method";
  mywrite( "PUT $method" );
  my $a = myread();
  die("Server didnt accept our PUT request")  if($a !~ /^OK/);

  my $xfer_began = time();
  my $sent = 0;
  eval {
     my $szar = Archive::SzarStream->write_handle($sock, {timeout=>$conf->{'timeout'},dir=>$conf->{'backupdir'}, compression=>$method, debug=>$conf->{'szar_debug'}, compression_level=>$conf->{'compression_level'}});
     for my $f (@$u) {
        my $filename = $f->{'dir'}.$f->{'name'};
        DEBUG "Sending $filename";
        if($szar->write_entry($filename, 0, 0, $f->{'mode'}, 0, $f->{'uid'}, $f->{'gid'}, $f->{'rdev'},
              $f->{'size'}, $f->{'atime'}, $f->{'mtime'}, 0, 0, 0, $f->{'t'})) {
          $sent++;
        } else {
          ERROR "Couldnt send $filename";
        }
     }
     $szar->write_eos();
     DEBUG "Sending ready";

     $sent_size += $szar->{'sent_bytes'};
     $xfer_sent_bytes += $szar->{'sent_bytes'};

  };
  die $@ if ( $@ );

  $xfer_sent_secs += time()-$xfer_began;

  if($sent != scalar @$u) {
    $proto_errors++;
    ERROR "We wanted to send ".(scalar @$u)." files, but managed $sent only";
  }

  $a = myread();
  die "Protocol error after PUT " if($a !~ /^OK (\d+)/m);
  my $servergot = $1;

  if($servergot != $sent) {
    $proto_errors++;
    ERROR "Server extracted $servergot files, we sent $sent";
  }
  return $servergot;
}

sub myuploads {
  my ($method, $c) = @_;

  mymoves($c) if(scalar @{$c->{'tomoves'}});
  myphantoms($c) if(scalar @{$c->{'tophantoms'}});

  myput($c->{"touploads_$method"}, $method);
  $dbh->commit;
  @{$c->{"touploads_$method"}} = ();
}
sub myadduploads {
  my ($c, $e, $fokat, $fullvirtualpath) = @_;

  my $method = $conf->{'put'};
  if((!$conf->{'put_filesize_fallback'})||($e->{'size'} < $conf->{'put_filesize_fallback'})) {
     if($e->{'name'} =~ /\.([a-z0-9]+)$/i) {
        my $ext = lc($1);
        for my $k (keys %$conf) {
           next if($k !~ /^put_exts_(.+)$/);
           my $m = $1;
           if($conf->{"put_exts_$m"}->{$ext}) {
              $method= $m;
              last;
           }
        }
     }
  }

      if($c->{'compareonly'}){
        INFO "$fokat $e->{'t'} $method: $fullvirtualpath";
        return;
      }

        DEBUG "$fokat $e->{'t'} $method: $fullvirtualpath";

  push @{$c->{"touploads_$method"}}, $e;
  myuploads($method, $c)  if(scalar @{$c->{"touploads_$method"}} >= $conf->{'upload_commit'});

  return $method;
}
sub cmd_rcheckpoints {
  parse_noparams();
  mywrite( "CPLIST" );
  my $r = trim(myread());
  die("No CP's at remote side") if($r !~ /^OK /);
  while($r =~ /(\d{14})/g) {
     INFO "CP: $1";
  }

}
sub cmd_backup {
  parse_noparams();

  my $compared = compare("inc");

  mywrite("CPEND");
  myread();


  showcomparestats($compared);
}
sub cmd_fullbackup {
  parse_noparams();

  my $compared = compare("full");

  mywrite("CPEND");
  myread();

  showcomparestats($compared);
}
sub parsets {
  my $ts = shift;

  my $tsd = "";

  if($ts=~/^(\d{4})(\d\d)(\d\d)(\d\d)(\d\d)(\d\d)$/) {
    $tsd = "$1-$2-$3 $4:$5:$6";
  } elsif($ts=~/^\d+$/) {
    $tsd = gc("SELECT ts FROM checkpoint WHERE id=?", $ts);
  } elsif($ts!~/^\d{4}-\d\d-\d\d \d\d:\d\d:\d\d$/) {
    my $tsdstr = "";
    $tsdstr = ",'$ts'" if($ts);
    $tsd = gc("SELECT DATETIME('now' $tsdstr)");
  } else {
    $tsd = $ts;
  }

  die("Cant calculate checkpoint time, probably invalid syntax")  if(!$tsd);

  return $tsd;
}
sub rcompare {
  my ($vdir, $ts, $human) = @_;

  die "Missing vdir"  if(!$vdir);
  die "Vdir must start with /" if($vdir !~ /^\//);

  $vdir .= '/';
  $vdir =~ s/\/+$/\//g;

  dbfetch();
  my $tsd = parsets($ts);
  INFO "Requested checkpoint time: $tsd";
  my $cp = gcaa("SELECT * FROM checkpoint WHERE ts <= '$tsd' ORDER BY ts DESC LIMIT 1 ");
  die "Sorry, no checkpoint earlier than the requested." if(!$cp->{'id'});
  INFO "Selected checkpoint $cp->{'id'} $cp->{'ts'}";

  my $orderby = "ORDER BY dir, name";
  $orderby = "ORDER BY checkpoint DESC, LENGTH(dir) DESC" if(!$human);

  my $latestcpid = gc("SELECT MAX(id) FROM checkpoint ORDER BY ts DESC LIMIT 1 ");
  my @cplist = (0);
  for(my $i = $cp->{'id'}; $i < $latestcpid; $i++) {
     push @cplist, $i;
  }
  my $cpliststr = join(",",@cplist);

  if($vdir =~ /^(.*\/)$/) {
    # directory selected
    my $rdir = $1;

    return mqwo("SELECT * FROM files WHERE checkpoint IN ($cpliststr) AND ocheckpoint <= ? AND dir LIKE ? $orderby",  $cp->{'id'}, "$rdir%");


# the upper version seems to be quicker
#    return mqwo("
#       SELECT * FROM files WHERE (checkpoint=0 OR checkpoint >= ?) AND ocheckpoint <= ? AND dir LIKE ? $orderby",
#    $cp->{'id'}, $cp->{'id'}, "$rdir%");
  }
  if($vdir =~ /^(.*\/)(.+)$/) {
    # file OR directory selected
    my ($rdir, $rfile) = ($1,$2);

  return mqwo( "

         SELECT * FROM files WHERE checkpoint IN ($cpliststr) AND ocheckpoint <= ? AND dir = ? AND name=?

      UNION
       (
          SELECT * FROM files WHERE checkpoint IN ($cpliststr) AND ocheckpoint <= ? AND dir LIKE ? $orderby
       )
      ",
        $cp->{'id'}, $rdir, $rfile,

       $cp->{'id'},"$rdir$rfile/%"
    );

# the upper version might be quicker
#    return mqwo( "
#
#         SELECT * FROM files WHERE (checkpoint=0 OR checkpoint >= ?) AND ocheckpoint <= ? AND dir=? AND name=?
#
#      UNION
#
#         SELECT * FROM files WHERE (checkpoint=0 OR checkpoint >= ?) AND ocheckpoint <= ? AND dir LIKE ? $orderby
#
#      ",
#        $cp->{'id'}, $cp->{'id'}, $rdir, $rfile,
#       $cp->{'id'}, $cp->{'id'}, "$rdir$rfile/%"
#    );
  }

  # this should not happen...
  die("invalid vdir pattern... report to dev");

}

sub cmd_rcompare {
  my $vdir = $ARGV[0];
  shift @ARGV;
  my $ts = join(" ", @ARGV);

  my $db = 0;
  my $sth = rcompare($vdir, $ts , 1);
  while(my $ref = $sth->fetchrow_hashref()) {
    $db++;
    INFO "$ref->{'ocheckpoint'}-$ref->{'checkpoint'}\t$ref->{'t'}\t$ref->{'dir'}$ref->{'name'}\n";
  }
  $sth->finish();

  INFO "Total items: $db";
}

sub restoreb {
  my($ddir,$vdir, $files) = @_;
  return if(!scalar @$files);
  myget($ddir, $files, $conf->{'get'}, $vdir);
}
sub cmd_cleanup {
  parse_noparams();

  mywrite("CLEANUP");
  my $x = myread();
  INFO $x;
}
sub cmd_delstalled {
  parse_noparams();

  mywrite("DELSTALLED");
  my $x = myread();
  INFO $x;
}
sub cmd_restore {
  my $vdir = $ARGV[0];
  my $ddir = $ARGV[1];
  shift @ARGV;
  shift @ARGV;
  my $ts = join(" ", @ARGV);

  $ddir =~ s/\/+$//g;

  my %f;
  $f{'restorebegin'} = time();
  $f{'restoredfiles'}=0;
  $f{'restoredfilesize'}=0;
  my $db = 0;
  my $alldb = 0;
  my $sth = rcompare($vdir, $ts);

  my $cddir = $ddir || ".";
  die("Destdir not exists: $ddir") if(!-d $cddir);
  die("Destdir is not writeable: $ddir") if(!-w $cddir);

  my @files;
  my $lref;
  while(my $ref = $sth->fetchrow_hashref()) {
    if($db >= $conf->{'restore_commit'}) {
      restoreb($ddir,$vdir, \@files);
      $db = 0;
      @files = ();
    }

    $db++;
    $alldb++;
    $lref = $ref;
    $f{'restoredfiles'}++;
    $f{'restoredfilesize'}+=$ref->{'size'};
    push @files, $ref;
  }
  $sth->finish();

  my $rvdir = $vdir;
  $rvdir = $lref->{'dir'} if(($alldb == 1)&&($db==1)&&($lref->{'t'}eq"F"));

  restoreb($ddir,$rvdir, \@files) if($db);
  $f{'restoreend'} = time();

  showrestorestats(\%f);
}

sub showrestorestats {
  my $c = shift;
  return  if(!$conf->{'showstats'});
  INFO "------------------------------------------ Restore";
  INFO "Elapsed time: ".showduration($c->{'restoreend'}-$c->{'restorebegin'}) if($c->{'restoreend'});
  INFO "Sessions: $sessions";
  INFO "Restored files: ".$c->{'restoredfiles'}. ", ".showsize($c->{'restoredfilesize'});
  showxferstats();
}
sub showxferstats {
  return  if(!$conf->{'showstats'});

  INFO "Full sent: ".showsize($sent_size);
  INFO "Full read: ".showsize($read_size);
  if($xfer_sent_secs) {
    INFO "File xfer sent dur: ".showduration($xfer_sent_secs);
    INFO "File xfer sent bytes: ".showsize($xfer_sent_bytes);
    INFO "File xfer sent speed: ".showspeed($xfer_sent_bytes/$xfer_sent_secs);
  }
  if($xfer_read_secs) {
    INFO "File xfer read dur: ".showduration($xfer_read_secs);
    INFO "File xfer read bytes: ".showsize($xfer_read_bytes);
    INFO "File xfer read speed: ".showspeed($xfer_read_bytes/$xfer_read_secs);
  }
}

