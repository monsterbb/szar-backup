FROM debian:buster

MAINTAINER tech@monstermedia.hu

ADD bin /opt/szar-backup/bin

RUN /opt/szar-backup/bin/szar-backup-install.sh
